# Instructions for creating a board plugin

The EMP plugin registers with the SWATCH framework commands and monitoring data that relate to the EMP firmware framework. It is designed as a base plugin library that provides abstract classes applicable to all boards which use the EMP framework. Board-specific plugins can then be created by building on top of the EMP plugin, inheriting from the abstract `emp::swatch::Processor` class and adding commands for performing tasks outside of the scope of the EMP framework (e.g. programming FPGAs, configuring/enabling voltage regulators, configuring clock synthesisers).

This document outlines the main classes and functions from the EMP plugin which need to be extended/invoked in order to create a plugin for a board that uses the EMP framework. Before reading this document, I strongly recommend reading the [general plugin development guide](https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development/index.html) - in particular the "Introduction", "Creating your plugin", "Controls", "Monitoring" and "Optical modules" pages.


## CI configuration file

In order to build on top of the results of this repository, you should set the `BASE_IMAGE_REPO_URL` variable as follows:
```yaml
  BASE_IMAGE_REPO_URL: gitlab-registry.cern.ch/p2-xware/software/emp-herd
```


## CMake configuration file

In order to link against the EMP plugin library, first add the following `find_package` command near the start of your config file:
```cmake
find_package(emp-swatch 0.3.10 REQUIRED)
```
You can then link against the EMP plugin by including `EMP::emp_swatch` in the `target_link_libraries` command.

For example, see https://gitlab.cern.ch/p2-xware/software/serenity-herd/-/blob/master/CMakeLists.txt


## Processor class

The EMP plugin contains a processor base class - `emp::swatch::Processor` - that inherits from `swatch::phase2::Processor` and registers all commands and monitoring objects/data which relate to the EMP framework firmware running on the main data-processing FPGAs (e.g. commands to reset the TTC interface & configure the MGTs, and the monitoring data related to those components). You should create a class that inherits from `emp::swatch::Processor`, and in your processor class's constructor register any board-specific commands associated with the main data-processing FPGA (e.g. commands for powering that FPGA on or off, programming that FPGA and configuring clock synths & Fireflys).

When calling the constructor of `emp::swatch::Processor`, you can choose from a few different constructor signatures:

 * A two-argument constructor:
   ```cpp
   Processor(const ::swatch::core::AbstractStub& aStub, const size_t aMaxNumPorts);
   ```
   The number of EMP I/O channels will be set to the specified value. In order to avoid any of the
   user interfaces displaying I/O channel indices for ports that could never exist, I recommend
   setting `aMaxNumPorts` to the max value for your FPGA/board (i.e. the value of `N_REGION` from
   the `emp_device_decl` package, multiplied by 4).

 * A three-argument constructor:
   ```cpp
   Processor(const ::swatch::core::AbstractStub& aStub, const size_t aMaxNumPorts, const size_t aCspIndexOffset);
   ```
   In this case, the CSP index offset is also specified in addition to the number of EMP I/O
   channels. The CSP index offset is added to the processor-local index of each output port when
   determining the its CSP channel index. It should only be set to a non-zero value on 2-FPGA board
   for which both FPGAs are have CSP inputs/outputs - and in that case it should be set to 0 for
   one FPGA and for the other FPGA it should be set to a non-zero value which ensures that the
   output ports on the two FPGAs have no overlap in their CSP channel indices.

 * A 6-argument constructor *(the recommended option)*:
   ```cpp
   Processor(const ::swatch::core::AbstractStub& aStub, const size_t aMaxNumPorts, const size_t aCspIndexOffset, const ConnectionCallback& aRxCallback, const InversionCallback& aRxCallback2, const ConnectionCallback& aTxCallback, const InversionCallback& aTxCallback2);
   ```
   where the callback typedefs are:
    * `ConnectionCallback`: `std::function<boost::optional<::swatch::phase2::ChannelID>(const size_t)>`
    * `InversionCallback`: `std::function<bool(const size_t)>`

   In this case, again the number of EMP I/O channels and CSP index offset will be set to their specified values.

   The connection callback functions should be used to declare which Firefly channel is connected to each RX/TX port on the FPGA. The `size_t` argument of a connection callback function is the index of the FPGA port, and the return value should be the name & index of the corresponding Firefly channel - or `boost::optional<::swatch::phase2::ChannelID>()` if that FPGA MGT is not connected to any Firefly site.
   
   The inversion callback functions declare whether or not the polarity needs to be inverted on any given FPGA MGT port. The `size_t` argument of an inversion callback function is again the index of the FPGA port, and the return value should be `true` for inverted polarity, or `false` for normal polarity.

In the constructor of your board-specific processor class (i.e. the class that inherits from `emp::swatch::Processor`),
if the board is in a crate, you should set the slot number and the crate's CSP ID by calling the
`setLogicalSlot(uint8_t)` and  `setCrateCSPID(uint8_t)` member functions. The values you give to these functions will
then be used for the 'source indentifier' information that is sent over the backend links in the spare (aka "filler")
bandwidth (this information is used to automatically verify that the link mapping between boards matches expectations).


## The 'program FPGA' command

When an FPGA is programmed with an EMP-based firmware image, beyond loading the firmware using board-specific software, some board-agnostic tasks also need to be performed. Before loading the firmware the `.tgz` has to be unpacked to extract the image file (e.g. `.bit` or `.svf`) and address tables. After loading the firmware the address tables used by the `emp::swatch::Processor` class need to be updated, and that class needs to read metadata from the firmware to adapt to the firmware settings used for that image (e.g. the set of MGT protocol blocks instantiated).

In order to minimise code duplication, we have created an abstract base class for the program command - `emp::swatch::commands::AbstractProgram`. This class implements the `code` method, which covers the above board-agnostic class, and then calls the pure virtual `program` method to actually load the firmware image. In your board-specific plugin you should create a `Program` class that inherits from `emp::swatch::commands::AbstractProgram`, and implement the following method which programs the FPGA using your board-specific procedures:
```cpp
  void program(const std::string& aProgrammingFilePath);
```
The FPGA should be fully programmed and the control bus route (PCIe / AXI chip-to-chip link) ready when this function returns (such that the EMP framework registers can be read and written immediately after the `program` function returns). The firmware image is passed to the `program` function as its first argument. You can select which type of firmware image file (e.g. `.bit` or `.svf`) is passed in through the third argument of the `AbstractProgram` constructor; if that argument is an empty string, then the path to the original `.tgz` file will be passed to the `program` function.

For an example of a program command based off the `AbstractProgram` class, see the serenity `Program` class at https://gitlab.cern.ch/p2-xware/software/serenity-herd/-/blob/master/include/serenity/swatch/commands/Program.hpp and https://gitlab.cern.ch/p2-xware/software/serenity-herd/-/blob/master/src/common/commands/Program.cpp


## Standard FSMs

The phase-2 SWATCH processor class has three standard FSMs (algo test, link test & slice test), which have the same set of transitions and states in all plugins. For EMP-based plugins, these transitions should contain a standard sequence of commands defined in the `emp::swatch::Processor` base class, but those EMP commands will need to be preceeded and/or followed by commands that are outside of the scope of the EMP plugin (e.g. to power on the FPGA & Fireflys, program the FPGA, configure clock synths & Fireflys).

We have created utiliy functions for populating the transitions of each FSM with EMP commands:
```cpp
void populateTransitions(::swatch::phase2::PayloadTestFSM&);
void populateTransitions(::swatch::phase2::LinkTestFSM&);
void populateTransitions(::swatch::phase2::SliceTestFSM&);
```
When these functions are run, they will add commands to the current end of the transitions, and so before/after running these functions in the processor class of a board-specific plugin, you should also add the board-specific commands for actions that are outside of the scope of the EMP plugin. The board-specific commands that should be added to each transition before/after running these functions are as follows:

| FSM           | Transition  | Before EMP commands                   | After EMP commands                    |
| ------------- | ----------- | ------------------------------------- | ------------------------------------- |
| Payload test  | setup       | Power on & program FPGA               | *None required*                       |
| Payload test  | configure   | *None required*                       | *None required*                       |
| Link test     | setup       | Power on, configure clock synths (for MGT refclks) & program FPGA | *None required*    |
| Link test     | configureTx |  *None required*                      | Firefly reset & configure TX Fireflys |
| Link test     | configureRx | Configure RX Fireflys                 | *None required*                       |
| Slice test    | setup       | Power on, configure clock synths (for MGT refclks) & program FPGA | *None required*    |
| Slice test    | configure   | Firefly reset & configure RX Fireflys | Configure TX Fireflys                 |


For an example of how these functions are used in practice in the Serenity plugin, see the `DaughterCard` constructor at https://gitlab.cern.ch/p2-xware/software/serenity-herd/-/blob/master/src/common/DaughterCard.cpp

#include "emp/swatch/utilities.hpp"


#include <algorithm>
#include <ctype.h>
#include <iostream>
#include <stdexcept>

#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

namespace emp {
namespace swatch {
namespace utilities {

std::vector<uint32_t> parseListOfIndices(const std::string& aList)
{
  std::vector<uint32_t> lResult;

  size_t lItemStartPosition = 0;
  size_t lNextCommaPosition = 0;

  while ((lItemStartPosition < aList.size()) and (lNextCommaPosition != std::string::npos)) {
    lNextCommaPosition = aList.find(',', lItemStartPosition);
    const size_t lDashPosition = std::min(aList.find('-', lItemStartPosition), lNextCommaPosition);

    if (lNextCommaPosition == lItemStartPosition)
      throw std::runtime_error("Index list \"" + aList + "\" has invalid syntax: Number missing before comma (at character " + std::to_string(lNextCommaPosition) + ")");

    // Single number
    if (lDashPosition == lNextCommaPosition) {
      const std::string lItem(aList.substr(lItemStartPosition, lNextCommaPosition - lItemStartPosition));
      if (not std::all_of(lItem.begin(), lItem.end(), isdigit))
        throw std::runtime_error("Index list \"" + aList + "\" has invalid syntax: Item \"" + lItem + "\" contains unexpected characters");

      lResult.push_back(std::stoi(lItem));
    }
    else {
      if ((lDashPosition == lItemStartPosition) or (lDashPosition == (lNextCommaPosition - 1)))
        throw std::runtime_error("Index list \"" + aList + "\" has invalid syntax: Item \"" + aList.substr(lItemStartPosition, lNextCommaPosition - lItemStartPosition) + "\" starts/ends with a dash");

      const std::string lPart1(aList.substr(lItemStartPosition, lDashPosition - lItemStartPosition));
      const std::string lPart2(aList.substr(lDashPosition + 1, lNextCommaPosition - lDashPosition - 1));
      if (not std::all_of(lPart1.begin(), lPart1.end(), isdigit) or not std::all_of(lPart2.begin(), lPart2.end(), isdigit))
        throw std::runtime_error("Index list \"" + aList + "\" has invalid syntax: Item \"" + aList.substr(lItemStartPosition, lNextCommaPosition - lItemStartPosition) + "\" contains unexpected characters");

      const size_t lIndex1 = std::stoi(lPart1);
      const size_t lIndex2 = std::stoi(lPart2);
      if (lIndex1 >= lIndex2)
        throw std::runtime_error("Index list \"" + aList + "\" has invalid syntax: Second index in item \"" + aList.substr(lItemStartPosition, lNextCommaPosition - lItemStartPosition) + "\" is not greater than first index");

      for (size_t i = lIndex1; i <= lIndex2; i++)
        lResult.push_back(i);
    }

    lItemStartPosition = lNextCommaPosition + 1;
  }

  return lResult;
}


FirmwareBuildProducts extractFirmwarePackage(const std::string& aPackagePath, const std::string& aProgrammingFileExtension)
{
  namespace fs = boost::filesystem;

  // 1) Create unique temporary directory
  fs::path lTempDir(fs::temp_directory_path());
  lTempDir /= fs::unique_path();
  try {
    fs::create_directories(lTempDir);
  }
  catch (std::exception& e) {
    throw std::runtime_error("Exception caught. Cannot create temporary dir \"" + lTempDir.native() + "\"");
  }

  // 2) Extract the tarball
  const std::string lExtractCommand("tar xzf " + aPackagePath + " -C " + lTempDir.native());
  int lErr = system(lExtractCommand.c_str());
  if (lErr != 0) {
    throw std::runtime_error("Bad return value (" + std::to_string(lErr) + ") from command \"" + lExtractCommand + "\"");
  }

  // 3a) Extract the IPBB package path
  std::vector<fs::path> lSubdirectories;
  fs::directory_iterator lEndIt;
  for (fs::directory_iterator lIt(lTempDir); lIt != lEndIt; lIt++) {
    if (fs::is_directory(lIt->path()))
      lSubdirectories.push_back(lIt->path());
  }

  if (lSubdirectories.size() != 1) {
    throw std::runtime_error("Unexpected number of directories (" + std::to_string(lSubdirectories.size()) + ") after extracting tarball");
  }
  fs::path lPackageDir(lSubdirectories.at(0));

  // 3b) Extract the programming file path
  std::vector<fs::path> lProgrammingFilePaths;
  if (aProgrammingFileExtension.empty())
    lProgrammingFilePaths.push_back(fs::path(aPackagePath.c_str()));
  else {
    for (fs::directory_iterator lIt(lPackageDir); lIt != lEndIt; lIt++) {
      if (lIt->path().extension() == aProgrammingFileExtension)
        lProgrammingFilePaths.push_back(lIt->path());
    }

    if (lProgrammingFilePaths.empty()) {
      throw std::runtime_error("No files with specified extension ('" + aProgrammingFileExtension + "') found in extracted package");
    }
    else if (lProgrammingFilePaths.size() > 1) {
      throw std::runtime_error(std::to_string(lSubdirectories.size()) + " programming files with extension '" + aProgrammingFileExtension + "' found in extracted package - not sure which one to choose!");
    }
  }
  FirmwareBuildProducts lResult;
  lResult.programmingFile = lProgrammingFilePaths.at(0).native();
  lResult.baseDirectory = lTempDir.string();

  // 3c) Extract the address table path
  const std::string kAddressTableName("top_emp.xml");
  const fs::path lAddrTablePath(lPackageDir / "addrtab" / kAddressTableName);

  if (not fs::is_regular_file(lAddrTablePath)) {
    throw std::runtime_error("Address table file '" + kAddressTableName + "' does not exist in extracted package");
  }

  lResult.addressTable = lAddrTablePath.native();

  // 3d) Find any other config files (e.g. the DTBO tarball for Apollo)
  for (auto& lEntry : boost::make_iterator_range(fs::directory_iterator(lPackageDir / "addrtab"), {})) {
    const auto lPath = lEntry.path();
    if (lPath.extension().native() != ".xml")
      lResult.additionalConfigFiles.push_back(lPath.native());
  }

  return lResult;
}

}
}
}

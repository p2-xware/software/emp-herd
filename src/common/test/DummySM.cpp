
#include "emp/swatch/test/DummySM.hpp"


#include "swatch/core/Factory.hpp"


SWATCH_REGISTER_CLASS(emp::swatch::test::DummySM)


namespace emp {
namespace swatch {
namespace test {

using namespace ::swatch;

DummySM::DummySM(const core::AbstractStub& aStub) :
  ServiceModule(aStub)
{
}


DummySM::~DummySM()
{
}


void DummySM::retrieveMetricValues()
{
}


} // namespace test
} // namespace swatch
} // namespace emp

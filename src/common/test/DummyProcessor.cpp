
#include "emp/swatch/test/DummyProcessor.hpp"


#include "swatch/core/Factory.hpp"


SWATCH_REGISTER_CLASS(emp::swatch::test::DummyProcessor)


namespace emp {
namespace swatch {
namespace test {

using namespace ::swatch;

DummyProcessor::DummyProcessor(const core::AbstractStub& aStub) :
  Processor(aStub, 128)
{
}


DummyProcessor::~DummyProcessor()
{
}


} // namespace test
} // namespace swatch
} // namespace emp


#include "emp/swatch/RxPort.hpp"


#include "emp/AlignMonNode.hpp"
#include "emp/ChannelBufferNode.hpp"
#include "emp/Controller.hpp"
#include "emp/DatapathNode.hpp"
#include "emp/MGTRegionNode.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace emp {
namespace swatch {

using namespace ::swatch;
using ::swatch::core::EqualCondition;
using ::swatch::core::GreaterThanCondition;

RxPort::RxPort(const size_t aIndex, const std::unique_ptr<emp::Controller>& aController, const boost::optional<phase2::ChannelID>& aConnectedChannel, bool aInvert) :
  InputPort(aIndex, createId(aIndex), aConnectedChannel),
  mController(aController),
  mInvertByDefault(aInvert),
  mBufferDepth(registerProperty<uint16_t>("Buffer depth")),
  mRefClockIndex(registerProperty<uint16_t>("Ref clock index")),
  mMGTType(registerProperty<std::string>("MGT type")),
  mLinkFirmwareType(registerProperty<std::string>("Link FW type")),
  mPolarity(registerProperty<std::string>("Polarity")),
  mEqualizerMode(registerProperty<std::string>("Equalizer mode")),

  mInitDone(registerMetric<bool>("initDone", "Initialisation complete", EqualCondition<bool>(false))),
  mStatus(registerMetric<bool>("status", "Status", EqualCondition<bool>(false))),
  mDownLatched(registerMetric<bool>("downLatched", "Down (latched)", EqualCondition<bool>(true))),
  mICMLockLost(registerMetric<bool>("ICMLockLost", "ICM lock lost", EqualCondition<bool>(true))),
  mFifoFull(registerMetric<bool>("fifoFull", "FIFO full", EqualCondition<bool>(true))),
  mFifoAlmostFull(registerMetric<bool>("fifoAlmostFull", "FIFO almost full")),
  mCounterPackets(registerMetric<uint32_t>("packetCount", "Packets in")),
  mCounterHdrErrors(registerMetric<uint16_t>("headerErrors", "Header errors")),
  mCounterTypeFieldSingleError(registerMetric<uint16_t>("controlTypeSingleErrorCount", "Control word type: Single-bit errors")),
  mCounterTypeFieldDoubleError(registerMetric<uint16_t>("controlTypeDoubleErrorCount", "Control word type: Double-bit errors")),
  mCounterControlWordWrongIndex(registerMetric<uint16_t>("controlWordWrongIndex", "Wrong index")),
  mCounterICMInvocations(registerMetric<uint16_t>("ICMInvocations", "ICM count")),
  // Alignment (orbit tag) status registers
  mOTTargetPosition(registerMetric<uint16_t>("OTTargetPosition", "OT target position")),
  mOTActualPosition(registerMetric<uint16_t>("OTActualPosition", "OT actual position")),
  mOrbitsSinceLastOTTargetPosition(registerMetric<uint16_t>("orbitsSinceLastOT", "Orbits since last OT", GreaterThanCondition<uint16_t>(0))),
  mOTOnTimeCount(registerMetric<uint32_t>("OTOnTimeCount", "On-time OTs", EqualCondition<uint32_t>(0))),
  mOTInvalidCount(registerMetric<uint32_t>("OTMissingCount", "Invalid OTs")),
  mOTMissingCount(registerMetric<uint32_t>("OTInvalidCount", "Missing OTs")),
  // PRBS status registers
  mPrbsLocked(registerMetric<bool>("prbsLocked", "Locked to PRBS stream?", EqualCondition<bool>(false))),
  mPrbsError(registerMetric<bool>("prbsError", "PRBS error (instantaneous)", EqualCondition<bool>(true))),
  mPrbsErrorCount(registerMetric<uint32_t>("prbsErrorCount", "PRBS error count"))
{
  setWarningCondition(mFifoAlmostFull, EqualCondition<bool>(true));
  setWarningCondition(mCounterPackets, EqualCondition<uint32_t>(0));
  setWarningCondition(mCounterTypeFieldSingleError, GreaterThanCondition<uint16_t>(0));
  setWarningCondition(mCounterTypeFieldDoubleError, GreaterThanCondition<uint16_t>(0));
  setWarningCondition(mCounterControlWordWrongIndex, GreaterThanCondition<uint16_t>(0));
  setWarningCondition(mCounterICMInvocations, GreaterThanCondition<uint16_t>(0));

  setWarningCondition(mOTInvalidCount, GreaterThanCondition<uint32_t>(0));
  setWarningCondition(mOTMissingCount, GreaterThanCondition<uint32_t>(0));

  setWarningCondition(mPrbsErrorCount, GreaterThanCondition<uint32_t>(0));
}


RxPort::~RxPort()
{
}


bool RxPort::invert() const
{
  return mInvertByDefault;
}


void RxPort::updateMetricSettings()
{
  const auto lCSPSetting(getOperatingMode() == phase2::kCSP ? core::monitoring::kEnabled : core::monitoring::kDisabled);
  const auto lPRBSSetting(getOperatingMode() == phase2::kPRBS ? core::monitoring::kEnabled : core::monitoring::kDisabled);

  // CSP metrics
  mMetricIsAligned.setMonitoringStatus(lCSPSetting);
  mMetricCRCErrors.setMonitoringStatus(lCSPSetting);

  mMetricSourceChannelId.setMonitoringStatus(lCSPSetting);
  mMetricSourceSlotId.setMonitoringStatus(lCSPSetting);
  mMetricSourceCrateId.setMonitoringStatus(lCSPSetting);

  mInitDone.setMonitoringStatus(lCSPSetting);
  mStatus.setMonitoringStatus(lCSPSetting);
  mDownLatched.setMonitoringStatus(lCSPSetting);
  mICMLockLost.setMonitoringStatus(lCSPSetting);
  mFifoFull.setMonitoringStatus(lCSPSetting);
  mFifoAlmostFull.setMonitoringStatus(lCSPSetting);

  mCounterPackets.setMonitoringStatus(lCSPSetting);
  mCounterHdrErrors.setMonitoringStatus(lCSPSetting);
  mCounterTypeFieldSingleError.setMonitoringStatus(lCSPSetting);
  mCounterTypeFieldDoubleError.setMonitoringStatus(lCSPSetting);
  mCounterControlWordWrongIndex.setMonitoringStatus(lCSPSetting);
  mCounterICMInvocations.setMonitoringStatus(lCSPSetting);

  mOTTargetPosition.setMonitoringStatus(lCSPSetting);
  mOTActualPosition.setMonitoringStatus(lCSPSetting);
  mOrbitsSinceLastOTTargetPosition.setMonitoringStatus(lCSPSetting);
  mOTOnTimeCount.setMonitoringStatus(lCSPSetting);
  mOTInvalidCount.setMonitoringStatus(lCSPSetting);
  mOTMissingCount.setMonitoringStatus(lCSPSetting);

  // PRBS metrics
  mPrbsLocked.setMonitoringStatus(lPRBSSetting);
  mPrbsError.setMonitoringStatus(lPRBSSetting);
  mPrbsErrorCount.setMonitoringStatus(lPRBSSetting);
}


std::string RxPort::createId(const size_t aIndex)
{
  std::ostringstream lOSS;
  lOSS << "Rx" << std::setw(2) << std::setfill('0') << aIndex;
  return lOSS.str();
}


bool RxPort::checkPresence() const
{
  if (not mController)
    return false;

  // 1) Check that EMP FW is loaded; bail out early if not
  const auto lMagic = mController->hw().getNode("info.magic").read();
  mController->hw().dispatch();
  if (lMagic.value() != 0xDEADBEEF)
    return false;

  // 2) Check that region exists in this FW build
  if (mIndex >= mController->getGenerics().nRegions * 4)
    return false;

  const auto lRegionInfo = mController->getDatapathDescriptor().getRegionInfo(mIndex / 4);
  return ((lRegionInfo.protocolIn == MGTProtocol::kCSP16) or (lRegionInfo.protocolIn == MGTProtocol::kCSP25));
}


void RxPort::retrievePropertyValues()
{
  if (not mController)
    return;

  const auto lRegionInfo = mController->getDatapathDescriptor().getRegionInfo(mIndex / 4);

  switch (lRegionInfo.mgtType) {
    case MGTKind::kNoGT:
      set<std::string>(mMGTType, "???");
      return;
    case MGTKind::kGTH:
      set<std::string>(mMGTType, "GTH");
      break;
    case MGTKind::kGTY:
      set<std::string>(mMGTType, "GTY");
      break;
    default:
      break;
  }

  switch (lRegionInfo.protocolIn) {
    case MGTProtocol::kNoProtocol:
      set<std::string>(mLinkFirmwareType, "Absent");
      return;
    case MGTProtocol::kCSP16:
      set<std::string>(mLinkFirmwareType, "CSP:16G");
      break;
    case MGTProtocol::kCSP25:
      set<std::string>(mLinkFirmwareType, "CSP:25G");
      break;
    default:
      break;
  }
  // TODO: Set mRefClockIndex

  mController->getDatapath().selectLink(mIndex);
  const auto& lMGTChannel = mController->getMGTs().getChannel(mIndex % 4);
  set<uint16_t>(mBufferDepth, lRegionInfo.bufIn == kBuffer ? mController->getBuffer().getBufferSize() : 0);
  set<std::string>(mPolarity, lMGTChannel.readRxPolarity() ? "Inverted" : "Normal");
  set<std::string>(mEqualizerMode, lMGTChannel.readDFEMode() == emp::MGTchannel::kDFE ? "DFE" : "LPM");
}


void RxPort::retrieveMetricValues()
{
  // Bail out early if controller hasn't been created yet (i.e. FPGA not programmed yet)
  if (not mController)
    return;

  mController->getDatapath().selectLink(mIndex);
  const auto lStatus = mController->getMGTs().getChannel(mIndex % 4).readRxStatus();

  setMetric(mMetricIsLocked, lStatus.qpllLocked);

  if (getOperatingMode() == ::swatch::phase2::kCSP) {
    const auto lSource = mController->getMGTs().getChannel(mIndex % 4).readRxLinkInfo();
    const auto lAlignStatus = mController->getAlignmentMonitor().readStatus();
    const auto lMetric = mController->getMetric();

    setMetric(mMetricIsAligned, lAlignStatus.latestPosition == lAlignStatus.targetPosition);
    setMetric(mMetricCRCErrors, lStatus.counters.crcErrors);

    setMetric(mMetricSourceChannelId, uint16_t(lSource.channel));
    setMetric(mMetricSourceSlotId, uint16_t(lSource.slot));
    setMetric(mMetricSourceCrateId, uint16_t(lSource.crate));

    setMetric(mInitDone, lStatus.initDone);
    setMetric(mStatus, lStatus.status);
    setMetric(mDownLatched, lStatus.downLatched);
    setMetric(mICMLockLost, lStatus.controlWordIndexLockLost);
    setMetric(mFifoFull, lStatus.fifoFull);
    setMetric(mFifoAlmostFull, lStatus.fifoAlmostFull);

    setMetric(mCounterPackets, lStatus.counters.packets);
    setMetric<uint16_t>(mCounterHdrErrors, lStatus.counters.headerErrors);
    setMetric<uint16_t>(mCounterTypeFieldSingleError, lStatus.counters.typeFieldSingleErrors);
    setMetric<uint16_t>(mCounterTypeFieldDoubleError, lStatus.counters.typeFieldDoubleErrors);
    setMetric<uint16_t>(mCounterControlWordWrongIndex, lStatus.counters.controlWordWrongIndex);
    setMetric<uint16_t>(mCounterICMInvocations, lStatus.counters.controlWordIndexLockLost);

    setMetric<uint16_t>(mOTTargetPosition, lMetric.bxsToCycles(lAlignStatus.targetPosition.bx) + lAlignStatus.targetPosition.cycle);
    setMetric<uint16_t>(mOTActualPosition, lMetric.bxsToCycles(lAlignStatus.latestPosition.bx) + lAlignStatus.latestPosition.cycle);
    setMetric<uint16_t>(mOrbitsSinceLastOTTargetPosition, lAlignStatus.orbitsSinceLastOT);
    setMetric<uint32_t>(mOTOnTimeCount, lAlignStatus.orbitTagOnTimeCount);
    setMetric<uint32_t>(mOTInvalidCount, lAlignStatus.orbitTagInvalidCount);
    setMetric<uint32_t>(mOTMissingCount, lAlignStatus.orbitTagMissingCount);
  }
  else {
    setMetric(mPrbsLocked, lStatus.prbsLocked);
    setMetric(mPrbsError, lStatus.prbsError);
    setMetric(mPrbsErrorCount, lStatus.prbsErrorCount);
  }
}



} // namespace swatch
} // namespace emp

#include "emp/swatch/commands/CheckTxMGTs.hpp"


#include "swatch/core/utilities.hpp"

#include "emp/ChannelManager.hpp"
#include "emp/Controller.hpp"

#include "emp/swatch/Processor.hpp"
#include "emp/swatch/utilities.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;


CheckTxMGTs::CheckTxMGTs(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Check TX BE MGTs", "Check that TX PRBS/CSP status registers have 'good' values", aActionable, std::string())
{
  // TODO: Replace ID channel spec with IOChannelSelector-type mechanism?
  //       (e.g. treating parameter as mask, adding ability to invert, auto-select subset with appropriate FW instantiated)
  registerParameter<std::string>({ "ids"s, "List of EMP channel indices, e.g. \"0,2,4-7\"" }, "");
}


CheckTxMGTs::~CheckTxMGTs()
{
}


action::Command::State CheckTxMGTs::code(const core::ParameterSet& aParams)
{
  Controller& lController = getActionable<Processor>().getController();

  const std::vector<uint32_t> lIds(utilities::parseListOfIndices(aParams.get<std::string>("ids")));

  ChannelManager lChannelManager(lController, lIds);

  setProgress(0.01, "Checking status of Tx MGTs " + core::shortVecFmt(lIds));

  try {
    const auto lTxStatusMap = lChannelManager.readTxStatus();
    const bool lGood = emp::checkTxMGTs(lTxStatusMap);

    setStatusMsg(lGood ? "Status of Tx MGTs: Good" : "MGT errors observed");
    return lGood ? State::kDone : State::kError;
  }
  catch (const std::exception& aExc) {
    std::ostringstream lErr;
    lErr << "Exception caught while trying to check status of Tx MGTs: " << aExc.what();
    setStatusMsg(lErr.str());
    return State::kError;
  }
}

} // namespace commands
} // namespace swatch
} // namespace emp

#include "emp/swatch/commands/EnablePayloadInputs.hpp"


#include "emp/Controller.hpp"
#include "emp/DatapathNode.hpp"

#include "emp/swatch/Processor.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace ::swatch;


EnablePayloadInputs::EnablePayloadInputs(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Enable payload inputs", "Enable all input channels (stop suppressing input data)", aActionable, std::string())
{
}


EnablePayloadInputs::~EnablePayloadInputs()
{
}


action::Command::State EnablePayloadInputs::code(const core::ParameterSet& aParams)
{
  Controller& lController = getActionable<Processor>().getController();

  setProgress(0.01, "Enabling all payload inputs");
  lController.getDatapath().enableInputs();

  return State::kDone;
}

} // namespace commands
} // namespace swatch
} // namespace emp

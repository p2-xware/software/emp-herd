#include "emp/swatch/commands/Reset.hpp"


#include <chrono>

#include "emp/Controller.hpp"
#include "emp/CtrlNode.hpp"
#include "emp/TTCNode.hpp"

#include "emp/swatch/Processor.hpp"


#include "boost/lexical_cast.hpp"

#include "swatch/core/rules/IsAmong.hpp"
#include "swatch/phase2/AlgoInterface.hpp"
#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"
#include "swatch/phase2/ReadoutInterface.hpp"
#include "swatch/phase2/TTCInterface.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;


const std::unordered_map<std::string, TTCMaster> kTTCMasterMap = {
  { "internal", TTCMaster::kInternal },
  { "legacy", TTCMaster::kLegacy },
  { "tcds2", TTCMaster::kTCDS2 }
};

Reset::Reset(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "", "Configure TCDS interface/generator and reset EMP firmware", aActionable, std::string())
{
  std::vector<std::string> lSourceNames;
  for (const auto& x : kTTCMasterMap)
    lSourceNames.push_back(x.first);
  registerParameter<std::string>("source", "internal", core::rules::IsAmong<std::string>(lSourceNames));
}


Reset::~Reset()
{
}


action::Command::State Reset::code(const core::ParameterSet& aParams)
{
  const TTCMaster lMaster = kTTCMasterMap.at(aParams.get<std::string>("source"));

  Controller& lController = getActionable<Processor>().getController();

  setProgress(0.01, "Resetting clocks (master=" + boost::lexical_cast<std::string>(lMaster) + ")");
  lController.getCtrl().softReset();
  lController.getTTC().configure(lMaster);

  setProgress(0.5, "Checking TTC status");
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  const bool lGood(checkTTC(lController.getTTC().readStatus()));

  setStatusMsg(lGood ? "TTC status: Good" : "Errors observed in TTC status registers");

  // Declare that only the TTC FW will be configured after this command
  using namespace ::swatch::phase2::processorIds;
  enableMonitoring(kTTC);
  disableMonitoring({ kReadout, kInputPorts, kOutputPorts, kTTC + ".legacy", kTTC + ".tcds2" });
  if (lMaster != TTCMaster::kInternal)
    enableMonitoring(kTTC + (lMaster == TTCMaster::kTCDS2 ? ".tcds2" : ".legacy"));
  for (auto& port : getActionable<Processor>().getInputPorts().getPorts())
    disableMonitoring(kInputPorts + "." + port->getId());
  for (auto& port : getActionable<Processor>().getOutputPorts().getPorts())
    disableMonitoring(kOutputPorts + "." + port->getId());

  return (lGood ? State::kDone : State::kError);
}

} // namespace commands
} // namespace swatch
} // namespace emp

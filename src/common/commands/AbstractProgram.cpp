
#include "emp/swatch/commands/AbstractProgram.hpp"


#include <stdlib.h>

#include <boost/filesystem.hpp>

#include "emp/swatch/Processor.hpp"
#include "emp/swatch/utilities.hpp"

#include "swatch/action/File.hpp"
#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"
#include "swatch/phase2/ReadoutInterface.hpp"
#include "swatch/phase2/TTCInterface.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace ::swatch;

AbstractProgram::AbstractProgram(const std::string& aId, action::ActionableObject& aActionable, const std::string& aProgrammingFileExtension) :
  Command(aId, "Program FPGA", aActionable, std::string()),
  mProgrammingFileExtension(aProgrammingFileExtension)
{
  registerParameter<action::File>("package", { "/path/to/package.tgz", "", "" });
}


AbstractProgram::~AbstractProgram()
{
}


action::Command::State AbstractProgram::code(const core::ParameterSet& aParams)
{
  // 1) Extract the tarball
  setProgress(0., "Extracting FW package");
  using emp::swatch::utilities::extractFirmwarePackage;
  const std::string& lPackagePath = aParams.get<action::File>("package").getPath();
  const auto lBuildProducts = extractFirmwarePackage(lPackagePath, mProgrammingFileExtension);

  /// add the list of files
  mListOfProgramFiles.push(lBuildProducts.baseDirectory);
  while (mListOfProgramFiles.size() > kMaxFiles) {
    boost::filesystem::remove_all(mListOfProgramFiles.front());
    mListOfProgramFiles.pop();
  }


  // 2) Program the FPGA (implemented by derived class, passing the .tgz path if mProgrammingFileExtension empty)
  this->program(lBuildProducts.programmingFile);

  // 3) Update address table used by EMP commands
  setProgress(0.8, "Updating address table");
  getActionable<Processor>().replaceController("file://" + lBuildProducts.addressTable);

  // 4) Read build metadata and run simple checks
  setProgress(0.9, "Reading build metadata");
  getActionable<Processor>().checkFirmware([&](const std::string& x) { return this->setStatusMsg(x); });

  // Declare none of the main FW components will be configured after this command
  using namespace ::swatch::phase2::processorIds;
  disableMonitoring({ kTTC, kReadout, kInputPorts, kOutputPorts });

  return State::kDone;
}


} // namespace commands
} // namespace swatch
} // namespace emp

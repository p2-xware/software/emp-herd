#include "emp/swatch/commands/ConfigureRxMGTs.hpp"


#include <chrono>
#include <thread>

#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>

#include "swatch/core/rules/All.hpp"
#include "swatch/core/rules/NonEmptyString.hpp"
#include "swatch/core/utilities.hpp"
#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"

#include "emp/AlignMonNode.hpp"
#include "emp/ChannelManager.hpp"
#include "emp/Controller.hpp"
#include "emp/MGTchannel.hpp"

#include "emp/swatch/Processor.hpp"
#include "emp/swatch/RxPort.hpp"
#include "emp/swatch/commands/SameSizeConstraint.hpp"
#include "emp/swatch/utilities.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;


ConfigureRxMGTs::ConfigureRxMGTs(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Configure RX BE MGTs", "Configure RX MGTs & backend link engines (CSP/PRBS)", aActionable, std::string())
{
  // TODO: Replace ID channel spec with IOChannelSelector-type mechanism?
  //       (e.g. treating parameter as mask, adding ability to invert, auto-select subset with appropriate FW instantiated)
  registerParameter<std::vector<std::string>>({ "ids"s, "List of EMP channel indices, e.g. \"0,2,4-7\"" }, std::vector<std::string>(), core::rules::All<std::string>(core::rules::NonEmptyString<std::string>()));
}


ConfigureRxMGTs::~ConfigureRxMGTs()
{
}


action::Command::State ConfigureRxMGTs::code(const core::ParameterSet& aParams)
{
  // Check that correct idle method has been requested
  std::vector<uint32_t> lPortsWithWrongIdleMethod;
  for (const auto& lIdsParam : aParams.get<std::vector<std::string>>("ids")) {
    const auto lIds = utilities::parseListOfIndices(lIdsParam);
    for (const size_t i : lIds) {
      const auto& lPort = getActionable<Processor>().getInputPorts().getPort(i);
      if ((lPort.getOperatingMode() == phase2::kCSP) and (lPort.getCSPSettings().idleMethod == phase2::CSPSettings::kIdleMethod2))
        lPortsWithWrongIdleMethod.push_back(i);
    }
  }
  if (not lPortsWithWrongIdleMethod.empty()) {
    setStatusMsg("Incorrect idle method (method 2) specified on input ports " + core::shortVecFmt(lPortsWithWrongIdleMethod) + "; EMP requires idle method 1");
    return State::kError;
  }

  // Reset monitoring status of input ports (monitoring of those configured will be enabled in for loop below)
  enableMonitoring(phase2::processorIds::kInputPorts);
  for (auto& port : getActionable<Processor>().getInputPorts().getPorts())
    disableMonitoring(phase2::processorIds::kInputPorts + "." + port->getId());

  auto lResult = State::kDone;
  const size_t lNumberOfGroups = aParams.get<std::vector<std::string>>("ids").size();
  for (size_t i = 0; i < lNumberOfGroups; i++) {
    const std::vector<uint32_t> lIds(utilities::parseListOfIndices(aParams.get<std::vector<std::string>>("ids").at(i)));
    // TODO: Read 'enable DFE' setting from config file or similar (like TX driver settings & Firefly Eq)
    const bool lEnableDFE(false);

    if (lIds.empty()) {
      setStatusMsg("Channel list is empty");
      return State::kError;
    }

    // Filter out masked & absent ports
    std::vector<uint32_t> lPortsToConfigure, lAbsentPorts, lMaskedPorts;
    for (const size_t i : lIds) {
      const auto& lPort = getActionable<Processor>().getInputPorts().getPort(i);
      if (not lPort.isPresent())
        lAbsentPorts.push_back(i);
      else if (lPort.isMasked())
        lMaskedPorts.push_back(i);
      else
        lPortsToConfigure.push_back(i);
    }

    // Declare that selected input ports will be configured by the end of this command
    for (auto& port : getActionable<Processor>().getInputPorts().getPorts()) {
      if ((std::count(lPortsToConfigure.begin(), lPortsToConfigure.end(), port->getIndex()) > 0) and port->isPresent()) {
        enableMonitoring(phase2::processorIds::kInputPorts + "." + port->getId());
        dynamic_cast<RxPort&>(*port).updateMetricSettings();
      }
    }

    // Report to users which channels are being configured
    std::ostringstream lMessageStream;
    lMessageStream << "Configuring Rx MGTs " << core::shortVecFmt(lPortsToConfigure);
    if ((not lAbsentPorts.empty()) or (not lMaskedPorts.empty())) {
      lMessageStream << " (";
      if (not lAbsentPorts.empty())
        lMessageStream << "absent channels: " << core::shortVecFmt(lAbsentPorts);
      if ((not lAbsentPorts.empty()) and (not lMaskedPorts.empty()))
        lMessageStream << "; ";
      if (not lMaskedPorts.empty())
        lMessageStream << "masked channels: " << core::shortVecFmt(lMaskedPorts);
      lMessageStream << ")";
    }
    setProgress(i * (1.0 / float(lNumberOfGroups)), lMessageStream.str());

    // Actually configure now
    const ::swatch::phase2::InputPortCollection& lPorts = getActionable<Processor>().getInputPorts();
    const std::function<bool(const uint32_t)> lInvertPolarity([&lPorts](const uint32_t i) { return ConfigureRxMGTs::invertPolarity(lPorts, i); });
    const std::function<bool(const uint32_t)> lEnableDFEFunction([&lEnableDFE](const uint32_t) { return lEnableDFE; });
    const std::function<MGTchannel::PRBSMode(const uint32_t)> lPRBSMode([&lPorts](const uint32_t i) { return ConfigureRxMGTs::getProtocol(lPorts, i); });

    Controller& lController = getActionable<Processor>().getController();
    ChannelManager lChannelManager(lController, lPortsToConfigure);
    lChannelManager.configureRxMGTs(lInvertPolarity, lEnableDFEFunction, true, lPRBSMode, true);

    // PART 2: CHECK STATUS
    // Must wait at least 255 packets beforehand (Min 1 packet/orbit, 10khz orbit rate => 0.0255s)
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    setProgress((i + 0.5) * (1.0 / float(lNumberOfGroups)), "Checking status of Rx MGTs " + core::shortVecFmt(lPortsToConfigure));
    try {
      const auto lRxStatusMap = lChannelManager.readRxStatus();
      const auto lAlignStatusMap = lChannelManager.readAlignmentStatus();

      // TODO: Reorganise so that can report the type of error (e.g. fundamental link/protocol failure, or just few CRCs)
      std::map<uint32_t, std::string> lBadChannelsMap, lConcerningChannelsMap;
      for (const auto x : lPortsToConfigure) {
        // Map MGT channel to connected module channel
        const auto& p = getActionable<Processor>().getInputPorts().getPort(x);
        std::string lConnectionName(p.getConnection()->component);
        std::replace(lConnectionName.begin(), lConnectionName.end(), '_', '.');
        const uint32_t lConnectionIndex(p.getConnection()->index);

        if (not emp::checkRxMGTs({ { x, lRxStatusMap.at(x) } }, false))
          lBadChannelsMap.insert(std::pair<uint32_t, std::string>(x, lConnectionName + " ch " + std::to_string(lConnectionIndex)));
        else if ((getProtocol(lPorts, x) == MGTchannel::kNoPRBS) and not emp::checkRxMGTs({ { x, lAlignStatusMap.at(x) } }, false))
          lBadChannelsMap.insert(std::pair<uint32_t, std::string>(x, lConnectionName + " ch " + std::to_string(lConnectionIndex)));
        else if (not emp::checkRxMGTs({ { x, lRxStatusMap.at(x) } }, true))
          lConcerningChannelsMap.insert(std::pair<uint32_t, std::string>(x, lConnectionName + " ch " + std::to_string(lConnectionIndex)));
        else if ((getProtocol(lPorts, x) == MGTchannel::kNoPRBS) and not emp::checkRxMGTs({ { x, lAlignStatusMap.at(x) } }, true))
          lConcerningChannelsMap.insert(std::pair<uint32_t, std::string>(x, lConnectionName + " ch " + std::to_string(lConnectionIndex)));
      }

      if (not lBadChannelsMap.empty()) {
        std::vector<uint32_t> lBadChannelsMGT;
        boost::copy(lBadChannelsMap | boost::adaptors::map_keys, std::back_inserter(lBadChannelsMGT));
        std::vector<std::string> lBadChannelsConnections;
        boost::copy(lBadChannelsMap | boost::adaptors::map_values, std::back_inserter(lBadChannelsConnections));
        setStatusMsg("MGT errors observed on channel" + std::string(lBadChannelsMap.size() > 1 ? "s " : " ") + core::shortVecFmt(lBadChannelsMGT) + " (" + core::join(lBadChannelsConnections, ", ") + ")");
      }
      if (not lConcerningChannelsMap.empty()) {
        std::vector<uint32_t> lConcerningChannelsMGT;
        boost::copy(lConcerningChannelsMap | boost::adaptors::map_keys, std::back_inserter(lConcerningChannelsMGT));
        std::vector<std::string> lConcerningChannelsConnections;
        boost::copy(lConcerningChannelsMap | boost::adaptors::map_values, std::back_inserter(lConcerningChannelsConnections));
        setStatusMsg("Transient errors detected on channel" + std::string(lConcerningChannelsMap.size() > 1 ? "s " : " ") + core::shortVecFmt(lConcerningChannelsMGT) + " (" + core::join(lConcerningChannelsConnections, ", ") + "), but they're still locked");
      }
      if (not lBadChannelsMap.empty())
        lResult = State::kError;
      else if ((not lConcerningChannelsMap.empty()) and (lResult != State::kError))
        lResult = State::kWarning;
    }
    catch (const std::exception& aExc) {
      std::ostringstream lErr;
      lErr << "Exception caught while trying to check status of Rx MGTs: " << aExc.what();
      setStatusMsg(lErr.str());
      lResult = State::kError;
    }
  }

  return lResult;
}


bool ConfigureRxMGTs::invertPolarity(const ::swatch::phase2::InputPortCollection& aPortCollection, uint32_t i)
{
  const auto& lPort = dynamic_cast<const RxPort&>(aPortCollection.getPort(i));
  return (not lPort.isInLoopback()) and lPort.invert();
}


MGTchannel::PRBSMode ConfigureRxMGTs::getProtocol(const ::swatch::phase2::InputPortCollection& aPortCollection, uint32_t i)
{
  const auto& lPort = aPortCollection.getPort(i);
  if (lPort.getOperatingMode() != phase2::kCSP) {
    switch (lPort.getPRBSMode()) {
      case phase2::kPRBS7:
        return MGTchannel::kPRBS7;
      case phase2::kPRBS9:
        return MGTchannel::kPRBS9;
      case phase2::kPRBS15:
        return MGTchannel::kPRBS15;
      case phase2::kPRBS23:
        return MGTchannel::kPRBS23;
      case phase2::kPRBS31:
        return MGTchannel::kPRBS31;
    }
  }

  return MGTchannel::kNoPRBS;
}



} // namespace commands
} // namespace swatch
} // namespace emp

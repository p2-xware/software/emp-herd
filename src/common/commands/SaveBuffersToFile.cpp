#include "emp/swatch/commands/SaveBuffersToFile.hpp"


#include "boost/filesystem.hpp"
#include "boost/lexical_cast.hpp"

#include "swatch/action/File.hpp"
#include "swatch/core/rules/NonEmptyString.hpp"
#include "swatch/core/utilities.hpp"

#include "emp/ChannelManager.hpp"
#include "emp/Controller.hpp"

#include "emp/swatch/Processor.hpp"
#include "emp/swatch/utilities.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;


template <RxTxSelector bKind>
SaveBuffersToFile<bKind>::SaveBuffersToFile(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "", "Read data from "s + (bKind == kRx ? "RX" : "TX") + " buffers", aActionable, std::string())
{
  // TODO: Replace ID channel spec with IOChannelSelector-type mechanism?
  //       (e.g. treating parameter as mask, adding ability to invert, auto-select subset with appropriate FW instantiated)
  registerParameter<std::string>({ "ids"s, "List of EMP channel indices, e.g. \"0,2,4-7\"" }, "");
}


template <RxTxSelector bKind>
SaveBuffersToFile<bKind>::~SaveBuffersToFile()
{
}


template <RxTxSelector bKind>
action::Command::State SaveBuffersToFile<bKind>::code(const core::ParameterSet& aParams)
{
  const std::vector<uint32_t> lIds(utilities::parseListOfIndices(aParams.get<std::string>("ids")));

  if (lIds.empty()) {
    setProgress(1.0, "Returning immediately, since channel list is empty");
    return State::kDone;
  }

  // 1) Create unique temporary directory
  namespace fs = boost::filesystem;
  fs::path lTempDir(fs::temp_directory_path());
  lTempDir /= fs::unique_path();

  try {
    fs::create_directories(lTempDir);
  }
  catch (std::exception& e) {
    setStatusMsg("Exception caught. Cannot create temporary dir \"" + lTempDir.native() + "\"");
    return State::kError;
  }

  // 2) Read buffers
  setProgress(0.2, "Reading buffers " + core::shortVecFmt(lIds));
  Controller& lController = getActionable<Processor>().getController();
  BoardData lData = ChannelManager(lController, lIds).readBuffers(bKind);

  // 3) Save data to file
  setProgress(0.6, "Saving data");
  const std::string lFileName((bKind == kRx ? "rx"s : "tx"s) + "_data.txt");
  fs::path lFilePath(lTempDir / fs::path(lFileName));
  BoardDataFactory::saveToFile(lData, lFilePath.native());

  setResult(action::File(lFilePath.native(), bKind == kRx ? phase2::fileTypes::kRxBufferData : phase2::fileTypes::kTxBufferData, "EMPv2"));
  return State::kDone;
}

template class SaveBuffersToFile<kRx>;
template class SaveBuffersToFile<kTx>;

} // namespace commands
} // namespace swatch
} // namespace emp

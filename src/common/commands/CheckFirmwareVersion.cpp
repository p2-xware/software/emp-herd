#include "emp/swatch/commands/CheckFirmwareVersion.hpp"


#include "uhal/Node.hpp"

#include "emp/Controller.hpp"
#include "emp/CtrlNode.hpp"

#include "emp/swatch/Processor.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace ::swatch;

CheckFirmwareVersion::CheckFirmwareVersion(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, std::string())
{
  registerParameter<uint32_t>("infraRev", 0);
  registerParameter<uint32_t>("algoRev", 0);
}


CheckFirmwareVersion::~CheckFirmwareVersion()
{
}


action::Command::State CheckFirmwareVersion::code(const core::ParameterSet& aParams)
{
  Controller& lController = getActionable<Processor>().getController();

  const uint32_t lExpectedInfraRev = aParams.get<uint32_t>("infraRev");
  const uint32_t lExpectedAlgoRev = aParams.get<uint32_t>("algoRev");

  uhal::ValWord<uint32_t> lFwInfraRev = lController.hw().getNode("info.versions.framework").read();
  uhal::ValWord<uint32_t> lFwAlgoRev = lController.hw().getNode("info.versions.payload").read();

  std::ostringstream lErrMsg;
  lErrMsg << std::hex;

  // Check for infra mismatch
  if (lExpectedInfraRev != (lFwInfraRev.value() & 0xffffff))
    lErrMsg << "Infra version mismatch - expected 0x" << lExpectedInfraRev << ", read 0x" << (lFwInfraRev.value() & 0xffffff) << ". ";

  if (lExpectedAlgoRev != (lFwAlgoRev.value()))
    lErrMsg << "Algo version mismatch - expected 0x" << lExpectedAlgoRev << ", read 0x" << lFwAlgoRev.value() << ".";

  if (lErrMsg.tellp() == 0)
    return State::kDone;

  setStatusMsg(lErrMsg.str());
  return State::kError;
}

} // namespace commands
} // namespace swatch
} // namespace emp

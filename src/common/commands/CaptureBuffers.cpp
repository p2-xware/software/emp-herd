#include "emp/swatch/commands/CaptureBuffers.hpp"


#include "swatch/core/utilities.hpp"

#include "emp/ChannelBufferNode.hpp"
#include "emp/ChannelManager.hpp"
#include "emp/Controller.hpp"
#include "emp/TTCNode.hpp"

#include "emp/swatch/Processor.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace ::swatch;


CaptureBuffers::CaptureBuffers(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Capture", "Trigger buffers to capture I/O data", aActionable, std::string())
{
}


CaptureBuffers::~CaptureBuffers()
{
}


action::Command::State CaptureBuffers::code(const core::ParameterSet& aParams)
{
  Controller& lController = getActionable<Processor>().getController();
  ChannelManager lChannelManager(lController);

  setProgress(0.01, "Clearing capture buffers");

  lChannelManager.clearBuffers(kRx, ChannelBufferNode::kCapture);
  lChannelManager.clearBuffers(kTx, ChannelBufferNode::kCapture);

  setProgress(0.5, "Capturing data stream");

  lController.getTTC().forceBTest();
  lChannelManager.waitCaptureDone();

  setStatusMsg("Capture completed");
  return State::kDone;
}

} // namespace commands
} // namespace swatch
} // namespace emp


#include "emp/swatch/commands/DisablePorts.hpp"


#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"

#include "emp/swatch/Processor.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace ::swatch;


DisablePorts::DisablePorts(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Disable I/O ports", "Disable monitoring for all I/O ports", aActionable, std::string())
{
}


DisablePorts::~DisablePorts()
{
}


action::Command::State DisablePorts::code(const core::ParameterSet& aParams)
{
  setProgress(0.01, "Disabling monitoring for all input & output ports");

  disableMonitoring(phase2::processorIds::kInputPorts);
  for (auto& port : getActionable<Processor>().getInputPorts().getPorts())
    disableMonitoring(phase2::processorIds::kInputPorts + "." + port->getId());

  disableMonitoring(phase2::processorIds::kOutputPorts);
  for (auto& port : getActionable<Processor>().getOutputPorts().getPorts())
    disableMonitoring(phase2::processorIds::kOutputPorts + "." + port->getId());

  return State::kDone;
}



} // namespace commands
} // namespace swatch
} // namespace emp

#include "emp/swatch/commands/AutoAlignRx.hpp"


#include "boost/lexical_cast.hpp"

#include "swatch/core/utilities.hpp"

#include "emp/ChannelManager.hpp"
#include "emp/Controller.hpp"
#include "emp/exception.hpp"

#include "emp/swatch/Processor.hpp"
#include "emp/swatch/utilities.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;


AutoAlignRx::AutoAlignRx(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Auto-align RX", "Align data from CSP links, relative to minimum achievable latency", aActionable, std::string())
{
  // TODO: Replace ID channel spec with IOChannelSelector-type mechanism?
  //       (e.g. treating parameter as mask, adding ability to invert, auto-select subset with appropriate FW instantiated)
  registerParameter<std::string>({ "ids"s, "List of EMP channel indices, e.g. \"0,2,4-7\"" }, "");

  registerParameter<uint32_t>("margin", 3);
}


AutoAlignRx::~AutoAlignRx()
{
}


action::Command::State AutoAlignRx::code(const core::ParameterSet& aParams)
{
  const std::vector<uint32_t> lIds(utilities::parseListOfIndices(aParams.get<std::string>("ids")));
  const uint32_t lMargin(aParams.get<uint32_t>("margin"));

  if (lIds.empty()) {
    setProgress(1.0, "Returning immediately, since channel list is empty");
    return State::kDone;
  }

  Controller& lController = getActionable<Processor>().getController();
  ChannelManager lChannelManager(lController, lIds);

  setProgress(0.01, "Starting auto-align for Rx MGTs " + core::shortVecFmt(lIds) + " with margin=" + boost::lexical_cast<std::string>(lMargin));

  try {
    lChannelManager.minimizeAndAlign(lMargin);
  }
  catch (const AlignmentFailed& lExc) {
    std::ostringstream lErr;
    lErr << "Alignment failed on input ports: " << core::joinAny(lExc.channels());
    setStatusMsg(lErr.str());
    return State::kError;
  }
  catch (const std::exception& lExc) {
    std::ostringstream lErr;
    lErr << "Exception caught in alignment: " << lExc.what();
    setStatusMsg(lErr.str());
    return State::kError;
  }

  setStatusMsg("Align MGTs completed for channels " + core::shortVecFmt(lIds));

  return State::kDone;
}

} // namespace commands
} // namespace swatch
} // namespace emp

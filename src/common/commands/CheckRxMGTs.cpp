#include "emp/swatch/commands/CheckRxMGTs.hpp"


#include "swatch/core/utilities.hpp"

#include "emp/AlignMonNode.hpp"
#include "emp/ChannelManager.hpp"
#include "emp/Controller.hpp"

#include "emp/swatch/Processor.hpp"
#include "emp/swatch/utilities.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;


CheckRxMGTs::CheckRxMGTs(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Check RX BE MGTs", "Check that RX PRBS/CSP status registers have 'good' values", aActionable, std::string())
{
  // TODO: Replace ID channel spec with IOChannelSelector-type mechanism?
  //       (e.g. treating parameter as mask, adding ability to invert, auto-select subset with appropriate FW instantiated)
  registerParameter<std::string>({ "ids"s, "List of EMP channel indices, e.g. \"0,2,4-7\"" }, "");
}


CheckRxMGTs::~CheckRxMGTs()
{
}


action::Command::State CheckRxMGTs::code(const core::ParameterSet& aParams)
{
  Controller& lController = getActionable<Processor>().getController();

  const std::vector<uint32_t> lIds(utilities::parseListOfIndices(aParams.get<std::string>("ids")));

  if (lIds.empty()) {
    setStatusMsg("Channel list is empty");
    return State::kError;
  }

  ChannelManager lChannelManager(lController, lIds);

  setProgress(0.01, "Checking status of Rx MGTs " + core::shortVecFmt(lIds));

  try {
    const auto lRxStatusMap = lChannelManager.readRxStatus();
    const auto lAlignStatusMap = lChannelManager.readAlignmentStatus();

    if (not(emp::checkRxMGTs(lRxStatusMap, false) and emp::checkRxMGTs(lAlignStatusMap, false))) {
      setStatusMsg("MGT errors observed");
      return State::kError;
    }

    const bool lPerfect = emp::checkRxMGTs(lRxStatusMap, false) and emp::checkRxMGTs(lAlignStatusMap, false);
    setStatusMsg(lPerfect ? "Status of Rx MGTs: Good" : "RX status currently good, but transient errors detected");
    return lPerfect ? State::kDone : State::kWarning;
  }
  catch (const std::exception& aExc) {
    std::ostringstream lErr;
    lErr << "Exception caught while trying to check status of Rx MGTs: " << aExc.what();
    setStatusMsg(lErr.str());
    return State::kError;
  }
}

} // namespace commands
} // namespace swatch
} // namespace emp

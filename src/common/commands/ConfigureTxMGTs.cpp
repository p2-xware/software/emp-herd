#include "emp/swatch/commands/ConfigureTxMGTs.hpp"

#include <chrono>
#include <thread>

#include "swatch/core/rules/All.hpp"
#include "swatch/core/rules/NonEmptyString.hpp"
#include "swatch/core/utilities.hpp"
#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"

#include "emp/ChannelManager.hpp"
#include "emp/Controller.hpp"
#include "emp/MGTchannel.hpp"

#include "emp/swatch/Processor.hpp"
#include "emp/swatch/TxPort.hpp"
#include "emp/swatch/commands/SameSizeConstraint.hpp"
#include "emp/swatch/utilities.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;


ConfigureTxMGTs::ConfigureTxMGTs(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Configure TX BE MGTs", "Configure TX MGTs & backend link engines (CSP/PRBS)", aActionable, std::string())
{
  // TODO: Replace ID channel spec with IOChannelSelector-type mechanism?
  //       (e.g. treating parameter as mask, adding ability to invert, auto-select subset with appropriate FW instantiated)
  registerParameter<std::vector<std::string>>({ "ids"s, "List of EMP channel indices, e.g. \"0,2,4-7\"" }, std::vector<std::string>(), core::rules::All<std::string>(core::rules::NonEmptyString<std::string>()));

  // // Declare constraints
  // addConstraint("sameSize", SameSizeConstraint({ "ids" }, {}, { "loopback" }));
}


ConfigureTxMGTs::~ConfigureTxMGTs()
{
}


action::Command::State ConfigureTxMGTs::code(const core::ParameterSet& aParams)
{
  const size_t lNumberOfGroups = aParams.get<std::vector<std::string>>("ids").size();

  for (size_t i = 0; i < lNumberOfGroups; i++) {
    const std::vector<uint32_t> lIds(utilities::parseListOfIndices(aParams.get<std::vector<std::string>>("ids").at(i)));

    if (lIds.empty()) {
      setStatusMsg("Channel list is empty");
      return State::kError;
    }

    // Filter out masked & absent ports
    std::vector<uint32_t> lPortsToConfigure, lMaskedPorts, lAbsentPorts;
    for (const size_t i : lIds) {
      const auto& lPort = getActionable<Processor>().getOutputPorts().getPort(i);
      if (not lPort.isPresent())
        lAbsentPorts.push_back(i);
      else if (lPort.isMasked())
        lMaskedPorts.push_back(i);
      else
        lPortsToConfigure.push_back(i);
    }

    // Declare that selected output ports will be configured by the end of this command
    enableMonitoring(phase2::processorIds::kOutputPorts);
    for (auto& port : getActionable<Processor>().getOutputPorts().getPorts()) {
      if ((std::count(lPortsToConfigure.begin(), lPortsToConfigure.end(), port->getIndex()) > 0) and port->isPresent())
        enableMonitoring(phase2::processorIds::kOutputPorts + "." + port->getId());
      else
        disableMonitoring(phase2::processorIds::kOutputPorts + "." + port->getId());
    }

    // Report to users which channels are being configured
    std::ostringstream lMessageStream;
    lMessageStream << "Configuring Tx MGTs " << core::shortVecFmt(lPortsToConfigure);
    if ((not lAbsentPorts.empty()) or (not lMaskedPorts.empty())) {
      lMessageStream << " (";
      if (not lAbsentPorts.empty())
        lMessageStream << "absent channels: " << core::shortVecFmt(lAbsentPorts);
      if ((not lAbsentPorts.empty()) and (not lMaskedPorts.empty()))
        lMessageStream << "; ";
      if (not lMaskedPorts.empty())
        lMessageStream << "masked channels: " << core::shortVecFmt(lMaskedPorts);
      lMessageStream << ")";
    }
    setProgress(i * (0.9 / float(lNumberOfGroups)), lMessageStream.str());

    // Actually configure now
    const ::swatch::phase2::OutputPortCollection& lPorts = getActionable<Processor>().getOutputPorts();
    const std::function<bool(const uint32_t)> lInvertPolarity([&lPorts](const uint32_t i) { return ConfigureTxMGTs::invertPolarity(lPorts, i); });
    const std::function<MGTchannel::LoopbackMode(const uint32_t)> lLoopbackMode([&lPorts](const uint32_t i) { return ConfigureTxMGTs::getLoopbackMode(lPorts, i); });
    const std::function<MGTchannel::PRBSMode(const uint32_t)> lPRBSMode([&lPorts](const uint32_t i) { return ConfigureTxMGTs::getProtocol(lPorts, i); });
    const std::function<CSPSettings(const uint32_t)> lCSP([&lPorts](const uint32_t i) { return ConfigureTxMGTs::getCSPSettings(lPorts, i); });

    Controller& lController = getActionable<Processor>().getController();
    ChannelManager lChannelManager(lController, lPortsToConfigure);
    lChannelManager.setBoardId(getActionable<Processor>().getCrateCSPID(), getActionable<Processor>().getLogicalSlot());
    lChannelManager.configureTxMGTs(lInvertPolarity, lLoopbackMode, lPRBSMode, lCSP);

    // PART 2: CHECK STATUS
    setProgress((i + 0.5) * (0.9 / float(lNumberOfGroups)), "Checking status of Tx MGTs " + core::shortVecFmt(lPortsToConfigure));
    try {
      const auto lTxStatusMap = lChannelManager.readTxStatus();
      const bool lGood = emp::checkTxMGTs(lTxStatusMap);

      setStatusMsg(lGood ? "Status of Tx MGTs: Good" : "MGT errors observed");
      if (not lGood)
        return State::kError;
    }
    catch (const std::exception& aExc) {
      std::ostringstream lErr;
      lErr << "Exception caught while trying to check status of Tx MGTs: " << aExc.what();
      setStatusMsg(lErr.str());
      return State::kError;
    }
  }

  // Sleep to allow Fireflies to reestablish lock
  setProgress(0.9, "Sleeping briefly to let optics lock");
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  return State::kDone;
}


bool ConfigureTxMGTs::invertPolarity(const ::swatch::phase2::OutputPortCollection& aPortCollection, uint32_t i)
{
  const auto& lPort = dynamic_cast<const TxPort&>(aPortCollection.getPort(i));
  return (not lPort.isInLoopback()) and lPort.invert();
}


MGTchannel::LoopbackMode ConfigureTxMGTs::getLoopbackMode(const ::swatch::phase2::OutputPortCollection& aPortCollection, uint32_t i)
{
  const auto& lPort = dynamic_cast<const TxPort&>(aPortCollection.getPort(i));
  return lPort.isInLoopback() ? MGTchannel::kNearPMA : MGTchannel::kNoLoopback;
}


MGTchannel::PRBSMode ConfigureTxMGTs::getProtocol(const ::swatch::phase2::OutputPortCollection& aPortCollection, uint32_t i)
{
  const auto& lPort = aPortCollection.getPort(i);
  if (lPort.getOperatingMode() != phase2::kCSP) {
    switch (lPort.getPRBSMode()) {
      case phase2::kPRBS7:
        return MGTchannel::kPRBS7;
      case phase2::kPRBS9:
        return MGTchannel::kPRBS9;
      case phase2::kPRBS15:
        return MGTchannel::kPRBS15;
      case phase2::kPRBS23:
        return MGTchannel::kPRBS23;
      case phase2::kPRBS31:
        return MGTchannel::kPRBS31;
    }
  }

  return MGTchannel::kNoPRBS;
}


CSPSettings ConfigureTxMGTs::getCSPSettings(const ::swatch::phase2::OutputPortCollection& aPortCollection, uint32_t i)
{
  const auto& lPort = aPortCollection.getPort(i);
  CSPSettings lSettings;

  if (lPort.getOperatingMode() == phase2::kCSP) {
    if (lPort.getCSPSettings().idleMethod == phase2::CSPSettings::kIdleMethod1)
      lSettings.idleMethod = CSPIdleMethod::kMethod1;
    else
      lSettings.idleMethod = CSPIdleMethod::kMethod2;
    if ((lPort.getCSPSettings().packetPeriodicity % 9) == 0)
      lSettings.packetInterval = lPort.getCSPSettings().packetPeriodicity;
    lSettings.packetSize = lPort.getCSPSettings().packetLength;
  }

  return lSettings;
}



} // namespace commands
} // namespace swatch
} // namespace emp

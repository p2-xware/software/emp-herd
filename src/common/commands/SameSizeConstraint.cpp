#include "emp/swatch/commands/SameSizeConstraint.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;


SameSizeConstraint::SameSizeConstraint(const std::vector<std::string>& aStringParameters, const std::vector<std::string>& aUIntParameters, const std::vector<std::string>& aBoolParameters) :
  mStringParameters(aStringParameters),
  mUIntParameters(aUIntParameters),
  mBoolParameters(aBoolParameters)
{
  for (const auto& x : aStringParameters)
    require<std::vector<std::string>>(x);
  for (const auto& x : aUIntParameters)
    require<std::vector<uint32_t>>(x);
  for (const auto& x : aBoolParameters)
    require<std::vector<bool>>(x);
}

SameSizeConstraint::~SameSizeConstraint()
{
}

void SameSizeConstraint::describe(std::ostream& aStream) const
{
  aStream << "allSameSize";
}

core::Match SameSizeConstraint::verify(const core::ParameterSet& aParams) const
{
  std::vector<size_t> lLengths;
  for (const auto& x : mStringParameters)
    lLengths.push_back(aParams.get<std::vector<std::string>>(x).size());
  for (const auto& x : mUIntParameters)
    lLengths.push_back(aParams.get<std::vector<uint32_t>>(x).size());
  for (const auto& x : mBoolParameters)
    lLengths.push_back(aParams.get<std::vector<bool>>(x).size());

  core::Match lResult(true);
  for (size_t i = 1; i < lLengths.size(); i++)
    lResult.ok = lResult.ok && (lLengths.at(i - 1) == lLengths.at(i));

  return lResult;
}

} // namespace commands
} // namespace swatch
} // namespace emp

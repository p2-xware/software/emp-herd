#include "emp/swatch/commands/DisablePayloadInputs.hpp"


#include "emp/Controller.hpp"
#include "emp/DatapathNode.hpp"

#include "emp/swatch/Processor.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace ::swatch;


DisablePayloadInputs::DisablePayloadInputs(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Disable payload inputs", "Disable all input channels (suppress input data)", aActionable, std::string())
{
}


DisablePayloadInputs::~DisablePayloadInputs()
{
}


action::Command::State DisablePayloadInputs::code(const core::ParameterSet& aParams)
{
  Controller& lController = getActionable<Processor>().getController();

  setProgress(0.01, "Suppressing all input data to payload");
  lController.getDatapath().disableInputs();

  return State::kDone;
}

} // namespace commands
} // namespace swatch
} // namespace emp

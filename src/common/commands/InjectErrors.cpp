#include "emp/swatch/commands/InjectErrors.hpp"

#include "emp/swatch/Processor.hpp"
#include "emp/swatch/TxPort.hpp"
#include "emp/swatch/utilities.hpp"

#include "emp/ChannelManager.hpp"
#include "emp/Controller.hpp"
#include "emp/definitions.hpp"

#include "swatch/core/rules/All.hpp"
#include "swatch/core/rules/IsAmong.hpp"
#include "swatch/core/rules/NonEmptyString.hpp"
#include "swatch/phase2/OutputPort.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"

namespace emp {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;

const std::unordered_map<std::string, BackendErrorType> kBackendErrorTypeMap = {
  { "prbs", BackendErrorType::kPRBS },
  { "crc", BackendErrorType::kCRC },
  { "index", BackendErrorType::kIndex },
  { "control-word-type-single-bitflip", BackendErrorType::kControlWordTypeSingleBitflip },
  { "control-word-type-double-bitflip", BackendErrorType::kControlWordTypeDoubleBitflip },
  { "header-single-bitflip", BackendErrorType::kHeaderSingleBitflip },
  { "header-double-bitflip", BackendErrorType::kHeaderDoubleBitflip }
};

InjectErrors::InjectErrors(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Inject errors", "Inject PRBS/CSP link errors at requested Tx ports", aActionable, std::string())
{
  registerParameter<std::string>({ "ids"s, "List of EMP channel indices, e.g. \"0,2,4-7\"" }, "");

  registerParameter<uint32_t>("count", 1);

  std::vector<std::string> lBackendErrorTypes;
  for (const auto& x : kBackendErrorTypeMap)
    lBackendErrorTypes.push_back(x.first);
  registerParameter<std::string>({ "errorType"s, "Error type" }, "prbs", core::rules::IsAmong<std::string>(lBackendErrorTypes));
}


InjectErrors::~InjectErrors()
{
}


action::Command::State InjectErrors::code(const core::ParameterSet& aParams)
{
  // Parse list of channel IDs.  Command will only act on unmasked channels.
  const std::vector<uint32_t> lIds(utilities::parseListOfIndices(aParams.get<std::string>("ids")));

  if (lIds.empty()) {
    setStatusMsg("Channel list is empty");
    return State::kError;
  }

  // Filter out masked & absent ports and return error if operating mode and error type do not match
  std::vector<uint32_t> lPortsToUse, lMaskedPorts, lAbsentPorts;
  const BackendErrorType lErrorType = kBackendErrorTypeMap.at(aParams.get<std::string>("errorType"));
  std::ostringstream lErrStream;
  for (const size_t i : lIds) {
    const auto& lPort = getActionable<Processor>().getOutputPorts().getPort(i);
    const auto& lPortMode = lPort.getOperatingMode();
    if (lPortMode != phase2::kCSP && lErrorType != BackendErrorType::kPRBS) {
      lErrStream << "Port " << i << " is operating in PRBS mode but CSP error type requested";
      setStatusMsg(lErrStream.str());
      return State::kError;
    }
    else if (lPortMode == phase2::kCSP && lErrorType == BackendErrorType::kPRBS) {
      lErrStream << "Port " << i << " is operating in CSP mode but PRBS error type requested";
      setStatusMsg(lErrStream.str());
      return State::kError;
    }
    else if (not lPort.isPresent())
      lAbsentPorts.push_back(i);
    else if (lPort.isMasked())
      lMaskedPorts.push_back(i);
    else
      lPortsToUse.push_back(i);
  }

  // Report channel IDs with injected errors
  const uint32_t lCount = aParams.get<uint32_t>("count");
  std::ostringstream lMessageStream;
  lMessageStream << "Injecting " << lCount << " " << aParams.get<std::string>("errorType") << " errors on Tx channels " << core::shortVecFmt(lPortsToUse);
  if ((not lAbsentPorts.empty()) or (not lMaskedPorts.empty())) {
    lMessageStream << " (";
    if (not lAbsentPorts.empty())
      lMessageStream << "absent channels: " << core::shortVecFmt(lAbsentPorts);
    if ((not lAbsentPorts.empty()) and (not lMaskedPorts.empty()))
      lMessageStream << "; ";
    if (not lMaskedPorts.empty())
      lMessageStream << "masked channels: " << core::shortVecFmt(lMaskedPorts);
    lMessageStream << ")";
  }
  setStatusMsg(lMessageStream.str());

  // Return error if all channels masked or absent
  if (lPortsToUse.empty()) {
    setStatusMsg("All channels absent or masked");
    return State::kError;
  }

  // Inject the errors
  Controller& lController = getActionable<Processor>().getController();
  ChannelManager lChannelManager(lController, lPortsToUse);

  for (size_t i = 0; i < size_t(lCount); i++) {
    lChannelManager.injectError(lErrorType);
  }

  return State::kDone;
}



} // namespace commands
} // namespace swatch
} // namespace emp

#include "emp/swatch/commands/ConfigureBuffers.hpp"


#include "boost/lexical_cast.hpp"

#include "swatch/action/File.hpp"
#include "swatch/core/PSetConstraint.hpp"
#include "swatch/core/rules/All.hpp"
#include "swatch/core/rules/IsAmong.hpp"
#include "swatch/core/rules/NonEmptyString.hpp"
#include "swatch/core/utilities.hpp"

#include "emp/ChannelManager.hpp"
#include "emp/Controller.hpp"
#include "emp/utilities/buffers.hpp"

#include "emp/swatch/Processor.hpp"
#include "emp/swatch/commands/SameSizeConstraint.hpp"
#include "emp/swatch/utilities.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;


template <RxTxSelector bKind>
const std::map<std::string, PathConfigurator::Mode> ConfigureBuffers<bKind>::kModeMap = {
  { "Latency", PathConfigurator::kLatency },
  { "Capture", PathConfigurator::kCapture },
  { "PlayOnce", PathConfigurator::kPlayOnce },
  { "PlayLoop", PathConfigurator::kPlayLoop },
  { "Pattern", PathConfigurator::kPattern },
  { "Zeroes", PathConfigurator::kZeroes },
  { "CaptureWhenStrobeHigh", PathConfigurator::kCaptureWhenStrobeHigh },
};


template <RxTxSelector bKind>
ConfigureBuffers<bKind>::ConfigureBuffers(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Configure " + std::string(bKind == emp::kRx ? "RX" : "TX ") + " buffers", "Configure I/O buffers and load data", aActionable, std::string())
{
  // TODO: Replace ID channel spec with IOChannelSelector-type mechanism?
  //       (e.g. treating parameter as mask, adding ability to invert, auto-select subset with appropriate FW instantiated)
  registerParameter<std::vector<std::string>>({ "ids"s, "List of EMP channel indices, e.g. \"0,2,4-7\"" }, std::vector<std::string>(), core::rules::All<std::string>(core::rules::NonEmptyString<std::string>()));

  std::vector<std::string> lModeNames;
  for (const auto& x : kModeMap)
    lModeNames.push_back(x.first);

  registerParameter<std::vector<std::string>>("dataURI", std::vector<std::string>());
  registerParameter<std::vector<action::File>>("dataFile", std::vector<action::File>());
  registerParameter<std::vector<std::string>>("mode", std::vector<std::string>(), core::rules::All<std::string>(core::rules::IsAmong<std::string>(lModeNames)));
  registerParameter<std::vector<uint32_t>>({ "startOffset"s, "Clock cycle at which playback/capture starts" }, std::vector<uint32_t>());

  // TODO: Add optional length, or stop BX & cycle parameters

  // TODO: Add checks on BX & cycle values

  // Declare constraints
  addConstraint("sameSize", SameSizeConstraint({ "ids", "dataURI", "mode" }, { "startOffset" }, {}));
}


template <RxTxSelector bKind>
ConfigureBuffers<bKind>::~ConfigureBuffers()
{
}


template <RxTxSelector bKind>
action::Command::State ConfigureBuffers<bKind>::code(const core::ParameterSet& aParams)
{
  const size_t lNumberOfGroups = aParams.get<std::vector<std::string>>("mode").size();

  for (size_t i = 0; i < lNumberOfGroups; i++) {
    const std::vector<uint32_t> lIds(utilities::parseListOfIndices(aParams.get<std::vector<std::string>>("ids").at(i)));
    std::string lPayload = aParams.get<std::vector<std::string>>("dataURI").at(i);
    const PathConfigurator::Mode lMode = kModeMap.at(aParams.get<std::vector<std::string>>("mode").at(i));
    const uint32_t lStartOffset = aParams.get<std::vector<uint32_t>>("startOffset").at(i);

    if (lIds.empty()) {
      setStatusMsg("Channel list is empty");
      return State::kError;
    }

    if ((lPayload == "file") or (lPayload == "file:") or (lPayload == "file://"))
      lPayload = "file://" + aParams.get<std::vector<action::File>>("dataFile").at(i).getPath();

    Controller& lController = getActionable<Processor>().getController();
    orbit::Metric lMetric = lController.getMetric();
    orbit::Point lStart(lStartOffset / lMetric.clockRatio(), lStartOffset % lMetric.clockRatio());

    // FIXME - Add generic solution within EMP
    // // Ensure that the start point is valid
    // lStart.throwIfInvalid();

    setProgress(i * (1.0 / float(lNumberOfGroups)), "Configuring buffers " + core::shortVecFmt(lIds) + " (mode=" + boost::lexical_cast<std::string>(lMode) + ", payload='" + lPayload + "', start=" + boost::lexical_cast<std::string>(lStart) + ")");

    configureBuffers(lController, bKind, lIds, lMode, lPayload, lStart, 0 /* end-of-orbit gap length */);
  }

  return State::kDone;
}


template class ConfigureBuffers<kRx>;
template class ConfigureBuffers<kTx>;

} // namespace commands
} // namespace swatch
} // namespace emp

#include "emp/swatch/commands/AlignRxTo.hpp"


#include "swatch/core/rules/All.hpp"
#include "swatch/core/rules/NonEmptyString.hpp"
#include "swatch/core/utilities.hpp"
#include "swatch/phase2/InputPort.hpp"
#include "swatch/phase2/InputPortCollection.hpp"

#include "emp/AlignMonNode.hpp"
#include "emp/ChannelManager.hpp"
#include "emp/Controller.hpp"
#include "emp/exception.hpp"

#include "emp/swatch/Processor.hpp"
#include "emp/swatch/commands/SameSizeConstraint.hpp"
#include "emp/swatch/utilities.hpp"


namespace emp {
namespace swatch {
namespace commands {

using namespace std::string_literals;
using namespace ::swatch;


AlignRxTo::AlignRxTo(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Align RX", "Align data from CSP links to pre-determined clock cycle", aActionable, std::string())
{
  // TODO: Replace ID channel spec with IOChannelSelector-type mechanism?
  //       (e.g. treating parameter as mask, adding ability to invert, auto-select subset with appropriate FW instantiated)
  registerParameter<std::vector<std::string>>({ "ids"s, "List of EMP channel indices, e.g. \"0,2,4-7\"" }, std::vector<std::string>(), core::rules::All<std::string>(core::rules::NonEmptyString<std::string>()));

  registerParameter<std::vector<uint32_t>>("target", std::vector<uint32_t>());

  // Declare constraints
  addConstraint("sameSize", SameSizeConstraint({ "ids" }, { "target" }, {}));
}


AlignRxTo::~AlignRxTo()
{
}


action::Command::State AlignRxTo::code(const core::ParameterSet& aParams)
{
  const size_t lNumberOfGroups = aParams.get<std::vector<std::string>>("ids").size();

  for (size_t i = 0; i < lNumberOfGroups; i++) {
    const std::vector<uint32_t> lIds(utilities::parseListOfIndices(aParams.get<std::vector<std::string>>("ids").at(i)));
    const uint32_t& lTarget = aParams.get<std::vector<uint32_t>>("target").at(i);

    if (lIds.empty()) {
      setStatusMsg("Channel list is empty");
      return State::kError;
    }

    // Check validity of target alignment point
    Controller& lController = getActionable<Processor>().getController();
    orbit::Metric lMetric = lController.getMetric();
    orbit::Point lTargetPoint(lTarget / lMetric.clockRatio(), lTarget % lMetric.clockRatio());

    // FIXME - Add generic solution within EMP
    // // Ensure that the alignment target is valid
    // lAlignTarget.throwIfInvalid();

    // Filter out masked, absent & non-CSP ports
    std::vector<uint32_t> lPortsToAlign, lAbsentPorts, lMaskedPorts, lPRBSPorts;
    for (const size_t i : lIds) {
      const auto& lPort = getActionable<Processor>().getInputPorts().getPort(i);
      if (not lPort.isPresent())
        lAbsentPorts.push_back(i);
      else if (lPort.isMasked())
        lMaskedPorts.push_back(i);
      else if (lPort.getOperatingMode() != phase2::kCSP)
        lPRBSPorts.push_back(i);
      else
        lPortsToAlign.push_back(i);
    }

    // Report to users which channels are being configured
    std::ostringstream lMessageStream;
    if (lPortsToAlign.empty())
      lMessageStream << "No ports to align ";
    else
      lMessageStream << "Aligning Rx MGTs " << core::shortVecFmt(lPortsToAlign);
    if ((not lAbsentPorts.empty()) or (not lMaskedPorts.empty()) or (not lPRBSPorts.empty())) {
      lMessageStream << "(";
      if (not lAbsentPorts.empty()) {
        lMessageStream << "absent channels: " << core::shortVecFmt(lAbsentPorts);
        if ((not lMaskedPorts.empty()) or (not lPRBSPorts.empty()))
          lMessageStream << "; ";
      }
      if (not lMaskedPorts.empty()) {
        lMessageStream << "masked channels: " << core::shortVecFmt(lMaskedPorts);
        if (not lPRBSPorts.empty())
          lMessageStream << "; ";
      }
      if (not lPRBSPorts.empty())
        lMessageStream << "PRBS channels: " << core::shortVecFmt(lPRBSPorts);
      lMessageStream << ")";
    }
    setProgress(i * (1.0 / float(lNumberOfGroups)), lMessageStream.str());

    // Exit early if all ports absent, masked, and/or not running in CSP mode
    if (lPortsToAlign.empty())
      return State::kDone;

    // Actually align now
    lMessageStream.str("");
    ChannelManager lChannelManager(lController, lPortsToAlign);
    try {
      lChannelManager.align(lTargetPoint);
    }
    catch (const AlignmentFailed& lExc) {
      if (not(checkRxMGTs(lChannelManager.readRxStatus(), false) and checkRxMGTs(lChannelManager.readAlignmentStatus(), false)))
        lMessageStream << "Status checks failed. ";
      lMessageStream << "Alignment failed on input ports: " << core::joinAny(lExc.channels());
      setStatusMsg(lMessageStream.str());
      return State::kError;
    }
    catch (const std::exception& lExc) {
      if (not(checkRxMGTs(lChannelManager.readRxStatus(), false) and checkRxMGTs(lChannelManager.readAlignmentStatus(), false)))
        lMessageStream << "Status checks failed. ";
      lMessageStream << "Exception caught in alignment: " << lExc.what();
      setStatusMsg(lMessageStream.str());
      return State::kError;
    }

    setStatusMsg("Rx channels " + core::shortVecFmt(lPortsToAlign) + " aligned");
  }

  return State::kDone;
}

} // namespace commands
} // namespace swatch
} // namespace emp

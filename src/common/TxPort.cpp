
#include "emp/swatch/TxPort.hpp"


#include "emp/ChannelBufferNode.hpp"
#include "emp/Controller.hpp"
#include "emp/DatapathNode.hpp"
#include "emp/InfoNode.hpp"
#include "emp/MGTRegionNode.hpp"
#include "emp/MGTTxDriverSettings.hpp"

#include "swatch/core/MetricConditions.hpp"


namespace emp {
namespace swatch {

using namespace ::swatch::core;


TxPort::TxPort(const size_t aIndex, const std::unique_ptr<emp::Controller>& aController, const boost::optional<::swatch::phase2::ChannelID>& aConnectedChannel, bool aInvert) :
  OutputPort(aIndex, createId(aIndex), aConnectedChannel),
  mController(aController),
  mInvertByDefault(aInvert),
  mBufferDepth(registerProperty<uint16_t>("Buffer depth")),
  mRefClockIndex(registerProperty<uint16_t>("Ref clock index")),
  mMGTType(registerProperty<std::string>("MGT type")),
  mLinkFirmwareType(registerProperty<std::string>("Link FW type")),
  mPolarity(registerProperty<std::string>("Polarity")),
  mDriverDiffSwing((addPropertyGroup("Driver settings"), registerProperty<float>("Differential swing", "Driver settings"))),
  mDriverPreCursor(registerProperty<float>("Pre-cursor pre-emphasis", "Driver settings")),
  mDriverPostCursor(registerProperty<float>("Post-cursor pre-emphasis", "Driver settings")),
  mMetricQpllLocked(registerMetric<bool>("QpllLocked", "QPLL locked", EqualCondition<bool>(false)))
{
  mDriverDiffSwing.setUnit("mV");
  mDriverDiffSwing.setFormat(format::kFixedPoint, 0);
  mDriverPreCursor.setUnit("dB");
  mDriverPreCursor.setFormat(format::kFixedPoint, 2);
  mDriverPostCursor.setUnit("dB");
  mDriverPostCursor.setFormat(format::kFixedPoint, 2);
}


TxPort::~TxPort()
{
}


bool TxPort::invert() const
{
  return mInvertByDefault;
}


std::string TxPort::createId(const size_t aIndex)
{
  std::ostringstream lOSS;
  lOSS << "Tx" << std::setw(2) << std::setfill('0') << aIndex;
  return lOSS.str();
}


bool TxPort::checkPresence() const
{
  if (not mController)
    return false;

  // 1) Check that EMP FW is loaded; bail out early if not
  const auto lMagic = mController->hw().getNode("info.magic").read();
  mController->hw().dispatch();
  if (lMagic.value() != 0xDEADBEEF)
    return false;

  // 2) Check that region exists in this FW build
  if (mIndex >= mController->getGenerics().nRegions * 4)
    return false;

  const auto lRegionInfo = mController->getDatapathDescriptor().getRegionInfo(mIndex / 4);
  return ((lRegionInfo.protocolOut == MGTProtocol::kCSP16) or (lRegionInfo.protocolOut == MGTProtocol::kCSP25));
}


void TxPort::retrievePropertyValues()
{
  if (not mController)
    return;

  const auto lRegionInfo = mController->getDatapathDescriptor().getRegionInfo(mIndex / 4);

  switch (lRegionInfo.mgtType) {
    case MGTKind::kNoGT:
      set<std::string>(mMGTType, "???");
      return;
    case MGTKind::kGTH:
      set<std::string>(mMGTType, "GTH");
      break;
    case MGTKind::kGTY:
      set<std::string>(mMGTType, "GTY");
      break;
    default:
      break;
  }

  switch (lRegionInfo.protocolOut) {
    case MGTProtocol::kNoProtocol:
      set<std::string>(mLinkFirmwareType, "Absent");
      return;
    case MGTProtocol::kCSP16:
      set<std::string>(mLinkFirmwareType, "CSP:16G");
      break;
    case MGTProtocol::kCSP25:
      set<std::string>(mLinkFirmwareType, "CSP:25G");
      break;
    default:
      break;
  }
  // TODO: Set mRefClockIndex

  mController->getDatapath().selectLink(mIndex);
  const auto& lMGTChannel = mController->getMGTs().getChannel(mIndex % 4);
  set<uint16_t>(mBufferDepth, lRegionInfo.bufOut == kBuffer ? mController->getBuffer().getBufferSize() : 0);
  set<std::string>(mPolarity, lMGTChannel.readTxPolarity() ? "Inverted" : "Normal");

  const auto lGen = mController->getInfo().readGeneration();
  const auto lSwing = emp::MGTTxDiffSwing::fromRegisterValue(lMGTChannel.readTxDriverDiffSwing(), lGen, lRegionInfo.mgtType);
  const auto lPre = emp::MGTTxPreCursor::fromRegisterValue(lMGTChannel.readTxDriverPreCursor(), lGen, lRegionInfo.mgtType);
  const auto lPost = emp::MGTTxPostCursor::fromRegisterValue(lMGTChannel.readTxDriverPostCursor(), lGen, lRegionInfo.mgtType);
  set(mDriverDiffSwing, float(lSwing.get()));
  set(mDriverPreCursor, lPre.get());
  set(mDriverPostCursor, lPost.get());
}


void TxPort::retrieveMetricValues()
{
  // Bail out early if controller hasn't been created yet (i.e. FPGA not programmed yet)
  if (not mController)
    return;

  mController->getDatapath().selectRegion(mIndex / 4);
  const auto lStatus = mController->getMGTs().getChannel(mIndex % 4).readTxStatus();

  setMetric(mMetricIsOperating, lStatus.initDone);
  setMetric(mMetricQpllLocked, lStatus.qpllLocked);
}


} // namespace swatch
} // namespace emp

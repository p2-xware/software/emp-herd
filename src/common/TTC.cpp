
#include "emp/swatch/TTC.hpp"


#include <sstream>

#include "emp/Controller.hpp"
#include "emp/TTCNode.hpp"

#include "swatch/core/MetricConditions.hpp"
#include "swatch/core/SimpleMetric.hpp"


using namespace swatch::core;

namespace emp {
namespace swatch {

TTC::LegacyInterface::LegacyInterface(const std::unique_ptr<emp::Controller>& aController) :
  MonitorableObject("legacy", "Legacy interface"),
  mMetricSingleBitErrors(registerMetric<uint32_t>("singleBitErrors", "Single-bit errors", GreaterThanCondition<uint32_t>(0))),
  mMetricDoubleBitErrors(registerMetric<uint32_t>("doubleBitErrors", "Double-bit errors", GreaterThanCondition<uint32_t>(0))),
  mController(aController)
{
}


TTC::LegacyInterface::~LegacyInterface()
{
}


void TTC::LegacyInterface::retrieveMetricValues()
{
  if (not mController)
    return;

  const auto lStatus = mController->getTTC().readStatus();

  if (not lStatus.legacy)
    return;

  setMetric(mMetricSingleBitErrors, lStatus.legacy->singleBitErrors);
  setMetric(mMetricDoubleBitErrors, lStatus.legacy->doubleBitErrors);
}


TTC::TCDS2Interface::TCDS2Interface(const std::unique_ptr<emp::Controller>& aController) :
  MonitorableObject("tcds2", "TCDS2 interface"),
  mMetricPowerGood(registerMetric<bool>("powerGood", "Power OK", EqualCondition<bool>(false))),
  mMetricRxPllLock(registerMetric<bool>("rxPLLLock", "RX PLL locked", EqualCondition<bool>(false))),
  mMetricRxReady(registerMetric<bool>("rxReady", "RX ready", EqualCondition<bool>(false))),
  mMetricRxResetDone(registerMetric<bool>("rxResetDone", "RX reset done", EqualCondition<bool>(false))),
  mMetricTxPllLock(registerMetric<bool>("txPLLLock", "TX PLL locked", EqualCondition<bool>(false))),
  mMetricTxReady(registerMetric<bool>("txReady", "TX ready", EqualCondition<bool>(false))),
  mMetricTxResetDone(registerMetric<bool>("txResetDone", "TX reset done", EqualCondition<bool>(false))),
  mMetricFrameLock(registerMetric<bool>("frameLock", "Frame locked", EqualCondition<bool>(false))),
  mMetricFrameUnlockCount(registerMetric<uint32_t>("frameUnlockCount", "Frame unlock count")),
  mController(aController)
{
}


TTC::TCDS2Interface::~TCDS2Interface()
{
}


void TTC::TCDS2Interface::retrieveMetricValues()
{
  if (not mController)
    return;

  const auto lStatus = mController->getTTC().readStatus();

  if (not lStatus.tcds2)
    return;

  setMetric(mMetricPowerGood, lStatus.tcds2->powerGood);
  setMetric(mMetricTxPllLock, lStatus.tcds2->txPllLock);
  setMetric(mMetricRxPllLock, lStatus.tcds2->rxPllLock);
  setMetric(mMetricTxResetDone, lStatus.tcds2->txResetDone);
  setMetric(mMetricRxResetDone, lStatus.tcds2->rxResetDone);
  setMetric(mMetricTxReady, lStatus.tcds2->txReady);
  setMetric(mMetricRxReady, lStatus.tcds2->rxReady);
  setMetric(mMetricFrameLock, lStatus.tcds2->frameLock);
  setMetric(mMetricFrameUnlockCount, lStatus.tcds2->frameUnlockCount);
}


TTC::TTC(const std::unique_ptr<emp::Controller>& aController) :
  TTCInterface(),
  mController(aController),
  mAvailableTtcSources(registerProperty<std::string>("Available sources")),
  mCurrentTtcClockSource(registerProperty<std::string>("Current source")),
  mMetricBC0OnTimeCount(registerMetric<uint32_t>("bc0OnTimeCount", "Valid BC0s received")),
  mMetricBC0MissingCount(registerMetric<uint32_t>("bc0MissingCount", "Missing BC0s")),
  mMetricBC0InvalidCount(registerMetric<uint32_t>("bc0InvalidCount", "Invalid BC0s received"))
{
  addMonitorable(new LegacyInterface(aController));
  addMonitorable(new TCDS2Interface(aController));
}


TTC::~TTC()
{
}


bool TTC::checkPresence() const
{
  return true;
}


void TTC::retrieveMetricValues()
{
  // Bail out early if controller hasn't been created yet (i.e. FPGA not programmed yet)
  if (not mController)
    return;

  const auto lStatus = mController->getTTC().readStatus();

  setMetric(mMetricL1ACounter, lStatus.eventCount);
  setMetric(mMetricBunchCounter, lStatus.bunchCount);
  setMetric(mMetricOrbitCounter, lStatus.orbitCount);
  setMetric(mMetricIsClock40Locked, lStatus.clockLock);
  setMetric(mMetricHasClock40Stopped, not lStatus.clockLock);
  setMetric(mMetricIsBC0Locked, lStatus.bc0Lock);

  setMetric(mMetricBC0OnTimeCount, lStatus.bc0OnTimeCount);
  setMetric(mMetricBC0MissingCount, lStatus.bc0MissingCount);
  setMetric(mMetricBC0InvalidCount, lStatus.bc0InvalidCount);
}


void TTC::retrievePropertyValues()
{
  if (not mController)
    return;

  // 1) Check that EMP FW is loaded; bail out early if not
  const auto lMagic = mController->hw().getNode("info.magic").read();
  mController->hw().dispatch();
  if (lMagic.value() != 0xDEADBEEF)
    return;

  // 2) Set the property values
  std::ostringstream lMsgStream;
  const auto lSources = mController->getTTC().readAvailableMasters();
  lMsgStream << lSources.at(0);
  for (size_t i = 1; i < lSources.size(); i++)
    lMsgStream << ", " << lSources.at(i);
  set(mAvailableTtcSources, lMsgStream.str());

  const auto lStatus = mController->getTTC().readStatus();
  if (lStatus.clockSource == lStatus.ttcSource)
    lMsgStream << lStatus.clockSource;
  else
    lMsgStream << "Clock: " << lStatus.clockSource << ". TTC: " << lStatus.ttcSource;
  set(mCurrentTtcClockSource, lMsgStream.str());
}


} // namespace swatch
} // namespace emp

#include "emp/swatch/Readout.hpp"


#include "emp/Controller.hpp"


namespace emp {
namespace swatch {

Readout::Readout(const std::unique_ptr<emp::Controller>& aController) :
  ReadoutInterface(),
  mController(aController)
{
}


Readout::~Readout()
{
}


void Readout::retrieveMetricValues()
{
  // Bail out early if controller hasn't been created yet (i.e. FPGA not programmed yet)
  if (not mController)
    return;
}


} // namespace swatch
} // namespace emp
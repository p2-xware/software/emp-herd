
#include "emp/swatch/Processor.hpp"


#include <iomanip>
#include <sstream>

#include "log4cplus/loggingmacros.h"

#include "boost/lexical_cast.hpp"

#include "uhal/ConnectionManager.hpp"

#include "emp/InfoNode.hpp"
#include "emp/logger/config.hpp"

#include "swatch/action/StateMachine.hpp"
#include "swatch/core/Factory.hpp"
#include "swatch/phase2/AlgoInterface.hpp"
#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"

#include "emp/swatch/Readout.hpp"
#include "emp/swatch/RxPort.hpp"
#include "emp/swatch/TTC.hpp"
#include "emp/swatch/TxPort.hpp"
#include "emp/swatch/commands/AlignRxTo.hpp"
#include "emp/swatch/commands/AutoAlignRx.hpp"
#include "emp/swatch/commands/CaptureBuffers.hpp"
#include "emp/swatch/commands/CheckFirmwareVersion.hpp"
#include "emp/swatch/commands/CheckRxMGTs.hpp"
#include "emp/swatch/commands/CheckTxMGTs.hpp"
#include "emp/swatch/commands/ConfigureBuffers.hpp"
#include "emp/swatch/commands/ConfigureRxMGTs.hpp"
#include "emp/swatch/commands/ConfigureTxMGTs.hpp"
#include "emp/swatch/commands/DisablePayloadInputs.hpp"
#include "emp/swatch/commands/DisablePorts.hpp"
#include "emp/swatch/commands/EnablePayloadInputs.hpp"
#include "emp/swatch/commands/InjectErrors.hpp"
#include "emp/swatch/commands/Reset.hpp"
#include "emp/swatch/commands/SaveBuffersToFile.hpp"


namespace emp {
namespace swatch {


using namespace ::swatch;


const std::string Processor::CmdIds::kChkFwRevision = "checkFwRevision";
const std::string Processor::CmdIds::kReset = "reset";
const std::string Processor::CmdIds::kDisablePayloadRx = "disablePayloadRx";
const std::string Processor::CmdIds::kDisablePorts = "disablePorts";
const std::string Processor::CmdIds::kEnablePayloadRx = "enablePayloadRx";
const std::string Processor::CmdIds::kCfgRxMGTs = "configureRxMGTs";
const std::string Processor::CmdIds::kCfgTxMGTs = "configureTxMGTs";
const std::string Processor::CmdIds::kChkRxMGTs = "checkRxMGTs";
const std::string Processor::CmdIds::kChkTxMGTs = "checkTxMGTs";
const std::string Processor::CmdIds::kAlignMGTs = "alignMGTs";
const std::string Processor::CmdIds::kAutoAlignMGTs = "autoAlignMGTs";
const std::string Processor::CmdIds::kCfgRxBuffers = "configureRxBuffers";
const std::string Processor::CmdIds::kCfgTxBuffers = "configureTxBuffers";
const std::string Processor::CmdIds::kCaptureBuffers = "capture";
const std::string Processor::CmdIds::kSaveRxBuffers = "saveRxBuffers";
const std::string Processor::CmdIds::kSaveTxBuffers = "saveTxBuffers";
const std::string Processor::CmdIds::kInjectErrors = "injectErrors";

std::once_flag Processor::sLogSetupOnceFlag;

const Processor::ConnectionCallback Processor::kDefaultConnectionCallback([](size_t i) { return boost::optional<phase2::ChannelID>(); });
const Processor::InversionCallback Processor::kDefaultInversionCallback([](size_t i) { return false; });


Processor::Processor(const core::AbstractStub& aStub, const size_t aMaxNumPorts) :
  Processor(aStub, aMaxNumPorts, 0, kDefaultConnectionCallback, kDefaultInversionCallback, kDefaultConnectionCallback, kDefaultInversionCallback)
{
}


Processor::Processor(const core::AbstractStub& aStub, const size_t aMaxNumPorts, const size_t aCspIndexOffset) :
  Processor(aStub, aMaxNumPorts, aCspIndexOffset, kDefaultConnectionCallback, kDefaultInversionCallback, kDefaultConnectionCallback, kDefaultInversionCallback)
{
}


Processor::Processor(const core::AbstractStub& aStub, const size_t aMaxNumPorts, const size_t aCspIndexOffset, const Processor::ConnectionCallback& aRxCallback, const Processor::InversionCallback& aRxCallback2, const Processor::ConnectionCallback& aTxCallback, const Processor::InversionCallback& aTxCallback2) :
  phase2::Processor(aStub, aCspIndexOffset),
  mLogicalSlot(0xF),
  mCrateCSPID(0xFF),
  mFpgaType((addPropertyGroup("Hardware"), registerProperty<std::string>("FPGA", "Hardware"))),
  mFpgaDNA(registerProperty<std::string>("FPGA DNA", "Hardware")),
  mPayloadClockFreq(registerProperty<float>("Payload clock frequency")),
  mBuildTime((addPropertyGroup("Build info"), registerProperty<std::string>("Build time", "Build info"))),
  mBuildType(registerProperty<std::string>("Build type", "Build info")),
  mBuildSourceAreas(registerProperty<action::Table>("Source areas", "Build info")),
  mTemperatureMetric(registerMetric<float>("temperature", "Temperature"))
{
  std::call_once(sLogSetupOnceFlag, Processor::setupLoggers);

  mPayloadClockFreq.setUnit("MHz");
  mPayloadClockFreq.setFormat(core::format::kFixedPoint, 0);
  mTemperatureMetric.setUnit("C");
  mTemperatureMetric.setFormat(core::format::kFixedPoint, 1);

  // NOTE: May want to uncomment this once reads of version & generic registers are removed from
  //       the Controller class' constructor - https://gitlab.cern.ch/p2-xware/software/emp-toolbox/-/issues/18
  // replaceController(getStub().addressTable);

  registerCommand<commands::Reset>(CmdIds::kReset);
  // registerCommand<commands::CheckFirmwareVersion>(CmdIds::kChkFwRevision);
  registerCommand<commands::DisablePayloadInputs>(CmdIds::kDisablePayloadRx);
  registerCommand<commands::DisablePorts>(CmdIds::kDisablePorts);
  registerCommand<commands::EnablePayloadInputs>(CmdIds::kEnablePayloadRx);

  registerCommand<commands::ConfigureRxBuffers>(CmdIds::kCfgRxBuffers);
  registerCommand<commands::ConfigureTxBuffers>(CmdIds::kCfgTxBuffers);
  registerCommand<commands::CaptureBuffers>(CmdIds::kCaptureBuffers);
  registerCommand<commands::SaveRxBuffersToFile>(CmdIds::kSaveRxBuffers);
  registerCommand<commands::SaveTxBuffersToFile>(CmdIds::kSaveTxBuffers);

  registerCommand<commands::ConfigureRxMGTs>(CmdIds::kCfgRxMGTs);
  registerCommand<commands::ConfigureTxMGTs>(CmdIds::kCfgTxMGTs);
  registerCommand<commands::CheckRxMGTs>(CmdIds::kChkRxMGTs);
  registerCommand<commands::CheckTxMGTs>(CmdIds::kChkTxMGTs);
  registerCommand<commands::AlignRxTo>(CmdIds::kAlignMGTs);
  registerCommand<commands::AutoAlignRx>(CmdIds::kAutoAlignMGTs);

  registerCommand<commands::InjectErrors>(CmdIds::kInjectErrors);

  registerReadout(new Readout(mController));

  registerTTC(new TTC(mController));

  auto& inputCollection = registerInputs();
  for (size_t i = 0; i < aMaxNumPorts; i++)
    inputCollection.addPort(new RxPort(i, mController, aRxCallback(i), aRxCallback2(i)));

  auto& outputCollection = registerOutputs();
  for (size_t i = 0; i < aMaxNumPorts; i++)
    outputCollection.addPort(new TxPort(i, mController, aTxCallback(i), aTxCallback2(i)));
}


Processor::~Processor()
{
}


emp::Controller& Processor::getController()
{
  if (not mController)
    throw std::runtime_error("Controller has not yet been registered");
  return *mController;
}


void Processor::replaceController(const std::string& aAddressTablePath)
{
  mController.reset(new Controller(uhal::ConnectionManager::getDevice(getStub().id, getStub().uri, aAddressTablePath)));
}


void Processor::removeController()
{
  mController.reset(NULL);
}


void Processor::checkFirmware(const std::function<void(const std::string&)>& aMessageCallback)
{
  const auto lMagic = getController().hw().getNode("info.magic").read();
  getController().hw().dispatch();
  if (lMagic.value() != 0xDEADBEEF) {
    std::ostringstream lMessage;
    lMessage << "Value of 'info.magic' register has incorrect value 0x";
    lMessage << std::hex << lMagic.value();
    lMessage << " (expected 0xDEADBEEF). This is not EMP firmware.";
    throw std::runtime_error(lMessage.str());
  }

  const auto lBuildInfo = getController().getInfo().readBuildInfo();

  std::ostringstream lMessage;
  lMessage << "Firmware was built at " << convertBuildTimeToString(lBuildInfo.buildTime);
  lMessage << ", from " << lBuildInfo.sourceAreas.size() << " source areas: ";
  for (size_t i = 0; i < lBuildInfo.sourceAreas.size(); i++) {
    const auto& lSourceArea = lBuildInfo.sourceAreas.at(i);
    lMessage << lSourceArea.name << " (";

    if (not lSourceArea.ref.empty())
      lMessage << lSourceArea.ref << ", ";
    lMessage << "commit " << std::hex << lSourceArea.sha;
    if (not lSourceArea.clean)
      lMessage << " + local changes)";
    else
      lMessage << ")";

    if (i < (lBuildInfo.sourceAreas.size() - 1))
      lMessage << "; ";
  }
  aMessageCallback(lMessage.str());

  if ((lBuildInfo.projectId == 0) and (lBuildInfo.pipelineId == 0) and (lBuildInfo.jobId == 0))
    aMessageCallback("FW build type: manual");
  else {
    lMessage = std::ostringstream();
    lMessage << "FW build type: Automated (GitLab CI) - from project " << lBuildInfo.projectId;
    lMessage << ", pipeline " << lBuildInfo.pipelineId;
    lMessage << ", job " << lBuildInfo.jobId;
    aMessageCallback(lMessage.str());
  }
}


void Processor::retrieveMetricValues()
{
  if (mController)
    setMetric(mTemperatureMetric, getController().getInfo().readTemperature());
}


uint8_t Processor::getLogicalSlot() const
{
  return mLogicalSlot;
}


uint8_t Processor::getCrateCSPID() const
{
  return mCrateCSPID;
}


void Processor::setCrateCSPID(uint8_t aID)
{
  mCrateCSPID = aID;
}


void Processor::setLogicalSlot(uint8_t aSlot)
{
  if ((aSlot < 1) or (aSlot > 14))
    throw std::runtime_error("Invalid logical slot number (" + std::to_string(uint16_t(aSlot)) + ") supplied to EMP Processor class; must be between 1 and 14");

  mLogicalSlot = aSlot;
}


void Processor::retrievePropertyValues()
{
  // 1) Check that EMP FW is loaded; bail out early if not
  if (not mController)
    return;
  const auto lMagic = getController().hw().getNode("info.magic").read();
  getController().hw().dispatch();
  if (lMagic.value() != 0xDEADBEEF)
    return;

  // 2) Hardware info
  const auto lDeviceInfo = getController().getInfo().readDeviceInfo();
  set(mFpgaType, boost::lexical_cast<std::string>(lDeviceInfo.fpga));
  set(mFpgaDNA, lDeviceInfo.dna);

  // 3) Framework build settings
  const auto lGenerics = getController().getGenerics();
  set<float>(mPayloadClockFreq, lGenerics.clockRatio * 40);

  // 4) FW build info
  const auto lBuildInfo = getController().getInfo().readBuildInfo();
  set(mBuildTime, convertBuildTimeToString(lBuildInfo.buildTime));
  if ((lBuildInfo.projectId == 0) and (lBuildInfo.pipelineId == 0) and (lBuildInfo.jobId == 0))
    set(mBuildType, std::string("Manual"));
  else
    set(mBuildType, "GitLab CI: Project " + std::to_string(lBuildInfo.projectId) + ", pipeline " + std::to_string(lBuildInfo.pipelineId) + ", job " + std::to_string(lBuildInfo.jobId));

  action::Table lSourceInfo({ { "Repository name", typeid(std::string) }, { "Branch/Tag", typeid(std::string) }, { "SHA", typeid(std::string) }, { "Uncommitted changes?", typeid(bool) } });
  for (const auto& x : lBuildInfo.sourceAreas) {
    std::ostringstream lSHA;
    lSHA << std::hex << "0x" << std::setfill('0') << std::setw(8) << x.sha;
    lSourceInfo.addRow({ { x.name, x.ref, lSHA.str(), x.clean } });
  }
  set(mBuildSourceAreas, lSourceInfo);
}


std::string Processor::convertBuildTimeToString(const BuildInfo::TimePoint_t& aTime)
{
  const std::time_t lBuildTime = std::chrono::system_clock::to_time_t(aTime);
  return boost::lexical_cast<std::string>(std::put_time(std::localtime(&lBuildTime), "%F %T %Z"));
}


void Processor::setupLoggers()
{
  uhal::setLogLevelTo(uhal::Notice());

  emp::logger::configure("EMP");
}

void populateTransitions(::swatch::phase2::PayloadTestFSM& aFSM)
{
  ::swatch::action::ActionableObject& lProcessor = aFSM.fsm.getActionable();

  aFSM.setup.add(lProcessor.getCommand(Processor::CmdIds::kReset));

  aFSM.playback
      .add(lProcessor.getCommand(Processor::CmdIds::kDisablePayloadRx))
      .add(lProcessor.getCommand(Processor::CmdIds::kCfgRxBuffers))
      .add(lProcessor.getCommand(Processor::CmdIds::kEnablePayloadRx))
      .add(lProcessor.getCommand(Processor::CmdIds::kCfgTxBuffers))
      .add(lProcessor.getCommand(Processor::CmdIds::kCaptureBuffers))
      .add(lProcessor.getCommand(Processor::CmdIds::kSaveRxBuffers))
      .add(lProcessor.getCommand(Processor::CmdIds::kSaveTxBuffers));
}


void populateTransitions(::swatch::phase2::LinkTestFSM& aFSM)
{
  ::swatch::action::ActionableObject& lProcessor = aFSM.fsm.getActionable();

  aFSM.setup.add(lProcessor.getCommand(Processor::CmdIds::kReset));

  aFSM.configureTx
      .add(lProcessor.getCommand(Processor::CmdIds::kCfgTxBuffers))
      .add(lProcessor.getCommand(Processor::CmdIds::kCfgTxMGTs));

  aFSM.configureRx
      .add(lProcessor.getCommand(Processor::CmdIds::kCfgRxMGTs))
      .add(lProcessor.getCommand(Processor::CmdIds::kAlignMGTs))
      .add(lProcessor.getCommand(Processor::CmdIds::kCfgRxBuffers));

  aFSM.capture
      .add(lProcessor.getCommand(Processor::CmdIds::kCaptureBuffers))
      .add(lProcessor.getCommand(Processor::CmdIds::kSaveRxBuffers))
      .add(lProcessor.getCommand(Processor::CmdIds::kSaveTxBuffers));

  using ::swatch::phase2::LinkTestFSM;
  aFSM.fsm.getTransition(LinkTestFSM::kStateRxConfigured, LinkTestFSM::kTrContinue)
      .add(lProcessor.getCommand(Processor::CmdIds::kDisablePayloadRx))
      .add(lProcessor.getCommand(Processor::CmdIds::kDisablePorts));
}


void populateTransitions(::swatch::phase2::SliceTestFSM& aFSM)
{
  ::swatch::action::ActionableObject& lProcessor = aFSM.fsm.getActionable();

  aFSM.setup.add(lProcessor.getCommand(Processor::CmdIds::kReset));

  aFSM.configure
      .add(lProcessor.getCommand(Processor::CmdIds::kCfgRxMGTs))
      .add(lProcessor.getCommand(Processor::CmdIds::kAlignMGTs))
      .add(lProcessor.getCommand(Processor::CmdIds::kCfgRxBuffers))
      .add(lProcessor.getCommand(Processor::CmdIds::kEnablePayloadRx))
      .add(lProcessor.getCommand(Processor::CmdIds::kCfgTxBuffers))
      .add(lProcessor.getCommand(Processor::CmdIds::kCfgTxMGTs));

  aFSM.capture
      .add(lProcessor.getCommand(Processor::CmdIds::kCaptureBuffers))
      .add(lProcessor.getCommand(Processor::CmdIds::kSaveRxBuffers))
      .add(lProcessor.getCommand(Processor::CmdIds::kSaveTxBuffers));

  using ::swatch::phase2::SliceTestFSM;
  aFSM.fsm.getTransition(SliceTestFSM::kStateConfigured, SliceTestFSM::kTrContinue)
      .add(lProcessor.getCommand(Processor::CmdIds::kDisablePayloadRx))
      .add(lProcessor.getCommand(Processor::CmdIds::kDisablePorts));
}


} // namespace swatch
} // namespace emp

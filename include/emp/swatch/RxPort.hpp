#ifndef __EMP_SWATCH_RXPORT_HPP__
#define __EMP_SWATCH_RXPORT_HPP__


#include <memory>

#include "swatch/core/SimpleMetric.hpp"
#include "swatch/phase2/InputPort.hpp"


namespace emp {

class Controller;

namespace swatch {

class RxPort : public ::swatch::phase2::InputPort {
public:
  RxPort(const size_t, const std::unique_ptr<emp::Controller>&, const boost::optional<::swatch::phase2::ChannelID>&, bool aInvert);

  ~RxPort();

  bool invert() const;

  void updateMetricSettings();

private:
  static std::string createId(const size_t);

  bool checkPresence() const final;

  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  const std::unique_ptr<emp::Controller>& mController;
  const bool mInvertByDefault;

  Property<uint16_t>& mBufferDepth;
  Property<uint16_t>& mRefClockIndex;
  Property<std::string>& mMGTType;
  Property<std::string>& mLinkFirmwareType;
  Property<std::string>& mPolarity;
  Property<std::string>& mEqualizerMode;

  // CSP metrics (TODO: Move to sub-object in future? would simplify updateMetricSettings, maybe also help in UI?)
  SimpleMetric<bool>& mInitDone;
  SimpleMetric<bool>& mStatus;
  SimpleMetric<bool>& mDownLatched;
  SimpleMetric<bool>& mICMLockLost;
  SimpleMetric<bool>& mFifoFull;
  SimpleMetric<bool>& mFifoAlmostFull;
  SimpleMetric<uint32_t>& mCounterPackets;
  SimpleMetric<uint16_t>& mCounterHdrErrors;
  SimpleMetric<uint16_t>& mCounterTypeFieldSingleError;
  SimpleMetric<uint16_t>& mCounterTypeFieldDoubleError;
  SimpleMetric<uint16_t>& mCounterControlWordWrongIndex;
  SimpleMetric<uint16_t>& mCounterICMInvocations;

  SimpleMetric<uint16_t>& mOTTargetPosition;
  SimpleMetric<uint16_t>& mOTActualPosition;
  SimpleMetric<uint16_t>& mOrbitsSinceLastOTTargetPosition;
  SimpleMetric<uint32_t>& mOTOnTimeCount;
  SimpleMetric<uint32_t>& mOTInvalidCount;
  SimpleMetric<uint32_t>& mOTMissingCount;

  // PRBS metrics (TODO: Move to sub-object in future? would simplify updateMetricSettings, maybe also help in UI?)
  SimpleMetric<bool>& mPrbsLocked;
  SimpleMetric<bool>& mPrbsError;
  SimpleMetric<uint32_t>& mPrbsErrorCount;
};

} // namespace swatch
} // namespace emp


#endif /* __EMP_SWATCH_RXPORT_HPP__ */
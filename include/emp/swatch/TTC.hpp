
#ifndef __EMP_SWATCH_TTC_HPP__
#define __EMP_SWATCH_TTC_HPP__


#include <memory>

#include "swatch/core/SimpleMetric.hpp"
#include "swatch/phase2/TTCInterface.hpp"


namespace emp {

class Controller;

namespace swatch {

class TTC : public ::swatch::phase2::TTCInterface {

  class LegacyInterface : public MonitorableObject {
  public:
    LegacyInterface(const std::unique_ptr<emp::Controller>&);
    virtual ~LegacyInterface();

  private:
    virtual void retrieveMetricValues();

    SimpleMetric<uint32_t>& mMetricSingleBitErrors;
    SimpleMetric<uint32_t>& mMetricDoubleBitErrors;

    const std::unique_ptr<emp::Controller>& mController;
  };

  class TCDS2Interface : public MonitorableObject {
  public:
    TCDS2Interface(const std::unique_ptr<emp::Controller>&);
    virtual ~TCDS2Interface();

  private:
    virtual void retrieveMetricValues();

    SimpleMetric<bool>& mMetricPowerGood;

    SimpleMetric<bool>& mMetricRxPllLock;
    SimpleMetric<bool>& mMetricRxReady;
    SimpleMetric<bool>& mMetricRxResetDone;
    SimpleMetric<bool>& mMetricTxPllLock;
    SimpleMetric<bool>& mMetricTxReady;
    SimpleMetric<bool>& mMetricTxResetDone;

    SimpleMetric<bool>& mMetricFrameLock;
    SimpleMetric<uint32_t>& mMetricFrameUnlockCount;

    const std::unique_ptr<emp::Controller>& mController;
  };

public:
  TTC(const std::unique_ptr<emp::Controller>&);
  virtual ~TTC();

private:
  bool checkPresence() const final;

  void retrieveMetricValues() final;

  void retrievePropertyValues() final;

  const std::unique_ptr<emp::Controller>& mController;

  ::swatch::action::Property<std::string>& mAvailableTtcSources;
  ::swatch::action::Property<std::string>& mCurrentTtcClockSource;

  SimpleMetric<uint32_t>& mMetricBC0OnTimeCount;
  SimpleMetric<uint32_t>& mMetricBC0MissingCount;
  SimpleMetric<uint32_t>& mMetricBC0InvalidCount;
};

} // namespace swatch
} // namespace emp

#endif /* __EMP_SWATCH_TTC_HPP__ */
#ifndef __EMP_SWATCH_COMMANDS_CHECKFIRMWAREVERSION__
#define __EMP_SWATCH_COMMANDS_CHECKFIRMWAREVERSION__


#include "swatch/action/Command.hpp"


namespace emp {
namespace swatch {
namespace commands {

class CheckFirmwareVersion : public ::swatch::action::Command {
public:
  CheckFirmwareVersion(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~CheckFirmwareVersion();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
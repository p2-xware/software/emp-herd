#ifndef __EMP_SWATCH_COMMANDS_CHECKRXMGTS__
#define __EMP_SWATCH_COMMANDS_CHECKRXMGTS__


#include "swatch/action/Command.hpp"


namespace emp {
namespace swatch {
namespace commands {

class CheckRxMGTs : public ::swatch::action::Command {
public:
  CheckRxMGTs(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~CheckRxMGTs();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
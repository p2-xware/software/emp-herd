#ifndef __EMP_SWATCH_COMMANDS_DISABLEPAYLOADINPUTS_HPP__
#define __EMP_SWATCH_COMMANDS_DISABLEPAYLOADINPUTS_HPP__


#include "swatch/action/Command.hpp"


namespace emp {
namespace swatch {
namespace commands {

class DisablePayloadInputs : public ::swatch::action::Command {
public:
  DisablePayloadInputs(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~DisablePayloadInputs();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
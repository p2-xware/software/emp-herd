#ifndef __EMP_SWATCH_COMMANDS_AUTOALIGNRX__
#define __EMP_SWATCH_COMMANDS_AUTOALIGNRX__


#include "swatch/action/Command.hpp"


namespace emp {
namespace swatch {
namespace commands {

class AutoAlignRx : public ::swatch::action::Command {
public:
  AutoAlignRx(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~AutoAlignRx();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
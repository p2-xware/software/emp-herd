#ifndef __EMP_SWATCH_COMMANDS_CHECKTXMGTS__
#define __EMP_SWATCH_COMMANDS_CHECKTXMGTS__


#include "swatch/action/Command.hpp"


namespace emp {
namespace swatch {
namespace commands {

class CheckTxMGTs : public ::swatch::action::Command {
public:
  CheckTxMGTs(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~CheckTxMGTs();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
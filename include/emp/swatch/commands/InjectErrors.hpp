#ifndef __EMP_SWATCH_COMMANDS_INJECTERRORS_HPP__
#define __EMP_SWATCH_COMMANDS_INJECTERRORS_HPP__


#include "swatch/action/Command.hpp"


namespace emp {
namespace swatch {
namespace commands {

class InjectErrors : public ::swatch::action::Command {
public:
  InjectErrors(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~InjectErrors();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
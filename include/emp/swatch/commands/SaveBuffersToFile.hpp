#ifndef __EMP_SWATCH_COMMANDS_SAVEBUFFERSTOFILE_HPP__
#define __EMP_SWATCH_COMMANDS_SAVEBUFFERSTOFILE_HPP__


#include "swatch/action/Command.hpp"

#include "emp/definitions.hpp"


namespace emp {
namespace swatch {
namespace commands {

template <RxTxSelector bKind>
class SaveBuffersToFile : public ::swatch::action::Command {
public:
  SaveBuffersToFile(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~SaveBuffersToFile();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

typedef SaveBuffersToFile<kRx> SaveRxBuffersToFile;
typedef SaveBuffersToFile<kTx> SaveTxBuffersToFile;

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
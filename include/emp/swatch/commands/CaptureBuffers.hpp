#ifndef __EMP_SWATCH_COMMANDS_CAPTUREBUFFERS_HPP__
#define __EMP_SWATCH_COMMANDS_CAPTUREBUFFERS_HPP__


#include "swatch/action/Command.hpp"


namespace emp {
namespace swatch {
namespace commands {

class CaptureBuffers : public ::swatch::action::Command {
public:
  CaptureBuffers(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~CaptureBuffers();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
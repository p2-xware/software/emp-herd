#ifndef __EMP_SWATCH_COMMANDS_CONFIGURERXMGTS__
#define __EMP_SWATCH_COMMANDS_CONFIGURERXMGTS__


#include "swatch/action/Command.hpp"

#include "emp/MGTchannel.hpp"


namespace swatch {
namespace phase2 {
class InputPortCollection;
}
}

namespace emp {
namespace swatch {
namespace commands {

class ConfigureRxMGTs : public ::swatch::action::Command {
public:
  ConfigureRxMGTs(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~ConfigureRxMGTs();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);

  static bool invertPolarity(const ::swatch::phase2::InputPortCollection&, uint32_t);

  static MGTchannel::PRBSMode getProtocol(const ::swatch::phase2::InputPortCollection&, uint32_t);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
#ifndef __EMP_SWATCH_COMMANDS_ABSTRACTPROGRAM_HPP__
#define __EMP_SWATCH_COMMANDS_ABSTRACTPROGRAM_HPP__


#include <queue>

#include "swatch/action/Command.hpp"


namespace emp {
namespace swatch {
namespace commands {

class AbstractProgram : public ::swatch::action::Command {
public:
  AbstractProgram(const std::string& aId, ::swatch::action::ActionableObject& aActionable, const std::string& aProgrammingFileExtension);

  virtual ~AbstractProgram();

private:
  State code(const ::swatch::core::ParameterSet& aParams) final;

  virtual void program(const std::string& aProgrammingFilePath) = 0;

  const std::string mProgrammingFileExtension;
  std::queue<std::string> mListOfProgramFiles;
  const size_t kMaxFiles = 10;
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif

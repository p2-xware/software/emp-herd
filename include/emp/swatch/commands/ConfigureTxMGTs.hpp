#ifndef __EMP_SWATCH_COMMANDS_CONFIGURETXMGTS__
#define __EMP_SWATCH_COMMANDS_CONFIGURETXMGTS__


#include "swatch/action/Command.hpp"

#include "emp/MGTchannel.hpp"

namespace swatch {
namespace phase2 {
class OutputPortCollection;
}
}

namespace emp {
namespace swatch {
namespace commands {

class ConfigureTxMGTs : public ::swatch::action::Command {
public:
  ConfigureTxMGTs(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~ConfigureTxMGTs();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);

  static bool invertPolarity(const ::swatch::phase2::OutputPortCollection&, uint32_t);

  static MGTchannel::LoopbackMode getLoopbackMode(const ::swatch::phase2::OutputPortCollection&, uint32_t);

  static MGTchannel::PRBSMode getProtocol(const ::swatch::phase2::OutputPortCollection&, uint32_t);

  static CSPSettings getCSPSettings(const ::swatch::phase2::OutputPortCollection&, uint32_t);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
#ifndef __EMP_SWATCH_COMMANDS_DISABLEPORTS_HPP__
#define __EMP_SWATCH_COMMANDS_DISABLEPORTS_HPP__


#include "swatch/action/Command.hpp"


namespace emp {
namespace swatch {
namespace commands {

class DisablePorts : public ::swatch::action::Command {
public:
  DisablePorts(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~DisablePorts();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
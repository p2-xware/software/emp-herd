#ifndef __EMP_SWATCH_COMMANDS_ENABLEPAYLOADINPUTS_HPP__
#define __EMP_SWATCH_COMMANDS_ENABLEPAYLOADINPUTS_HPP__


#include "swatch/action/Command.hpp"


namespace emp {
namespace swatch {
namespace commands {

class EnablePayloadInputs : public ::swatch::action::Command {
public:
  EnablePayloadInputs(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~EnablePayloadInputs();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
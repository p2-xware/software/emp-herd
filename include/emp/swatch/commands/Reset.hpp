#ifndef __EMP_SWATCH_COMMANDS_RESET__
#define __EMP_SWATCH_COMMANDS_RESET__


#include "swatch/action/Command.hpp"


namespace emp {
namespace swatch {
namespace commands {

class Reset : public ::swatch::action::Command {
public:
  Reset(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~Reset();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
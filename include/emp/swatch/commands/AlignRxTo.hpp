#ifndef __EMP_SWATCH_COMMANDS_ALIGNRXTO__
#define __EMP_SWATCH_COMMANDS_ALIGNRXTO__


#include "swatch/action/Command.hpp"


namespace emp {
namespace swatch {
namespace commands {

class AlignRxTo : public ::swatch::action::Command {
public:
  AlignRxTo(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~AlignRxTo();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);
};

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
#ifndef __EMP_SWATCH_COMMANDS_CONFIGURERXBUFFERS_HPP__
#define __EMP_SWATCH_COMMANDS_CONFIGURERXBUFFERS_HPP__


#include "swatch/action/Command.hpp"

#include "emp/PathConfigurator.hpp"
#include "emp/definitions.hpp"


namespace emp {
namespace swatch {
namespace commands {

template <RxTxSelector bKind>
class ConfigureBuffers : public ::swatch::action::Command {
public:
  ConfigureBuffers(const std::string& aId, ::swatch::action::ActionableObject& aActionable);

  virtual ~ConfigureBuffers();

private:
  virtual State code(const ::swatch::core::ParameterSet& aParams);

  static const std::map<std::string, PathConfigurator::Mode> kModeMap;
};

typedef ConfigureBuffers<kRx> ConfigureRxBuffers;
typedef ConfigureBuffers<kTx> ConfigureTxBuffers;

} // namespace commands
} // namespace swatch
} // namespace emp

#endif
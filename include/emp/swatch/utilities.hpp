#ifndef __EMP_SWATCH_UTILITIES__
#define __EMP_SWATCH_UTILITIES__


#include <stddef.h>
#include <string>
#include <vector>


namespace emp {
namespace swatch {
namespace utilities {

struct FirmwareBuildProducts {
  // Path to firmware binary (.bit, .svf, etc)
  std::string programmingFile;
  // Path to address table
  std::string addressTable;
  // Additional config files (if any)
  std::vector<std::string> additionalConfigFiles;
  // Path to the base directory where .tgz program files are stored - to remove
  std::string baseDirectory;
};

std::vector<uint32_t> parseListOfIndices(const std::string&);

FirmwareBuildProducts extractFirmwarePackage(const std::string& aPackagePath, const std::string& aProgrammingFileExtension);

}
}
}

#endif

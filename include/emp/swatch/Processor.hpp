
#ifndef __EMP_SWATCH_EMPDEVICE_HPP__
#define __EMP_SWATCH_EMPDEVICE_HPP__


#include <memory>
#include <mutex>

#include "emp/Controller.hpp"

#include "swatch/action/Table.hpp"
#include "swatch/phase2/ChannelID.hpp"
#include "swatch/phase2/Processor.hpp"


namespace emp {
namespace swatch {


class Processor : public ::swatch::phase2::Processor {
public:
  typedef std::function<boost::optional<::swatch::phase2::ChannelID>(const size_t)> ConnectionCallback;
  typedef std::function<bool(const size_t)> InversionCallback; // TRUE -> INVERT BY DEFAULT

  struct CmdIds {
    static const std::string kChkFwRevision;
    static const std::string kReset;
    static const std::string kDisablePayloadRx;
    static const std::string kDisablePorts;
    static const std::string kEnablePayloadRx;
    static const std::string kCfgRxMGTs;
    static const std::string kCfgTxMGTs;
    static const std::string kChkRxMGTs;
    static const std::string kChkTxMGTs;
    static const std::string kAlignMGTs;
    static const std::string kAutoAlignMGTs;
    static const std::string kCfgRxBuffers;
    static const std::string kCfgTxBuffers;
    static const std::string kCaptureBuffers;
    static const std::string kSaveRxBuffers;
    static const std::string kSaveTxBuffers;
    static const std::string kInjectErrors;
  };

  Processor(const ::swatch::core::AbstractStub& aStub, const size_t aMaxNumPorts);
  Processor(const ::swatch::core::AbstractStub& aStub, const size_t aMaxNumPorts, const size_t aCspIndexOffset);
  Processor(const ::swatch::core::AbstractStub& aStub, const size_t aMaxNumPorts, const size_t aCspIndexOffset, const ConnectionCallback& aRxCallback, const InversionCallback& aRxCallback2, const ConnectionCallback& aTxCallback, const InversionCallback& aTxCallback2);
  virtual ~Processor();

  emp::Controller& getController();

  // Replaces emp::Controller class to use specified address table; should be called whenever a bitfile is loaded
  void replaceController(const std::string& aAddressTablePath);

  // Resets controller pointer to null - should be called when FPGA is powered off or unprogrammed
  void removeController();

  // Reads 'info' registers containing build meta data (may check version compatibility in future); should be called whenever a bitfile is loaded
  void checkFirmware(const std::function<void(const std::string&)>& aMessageCallback);

  void retrieveMetricValues() override;

  //! Returns logical slot, used for metadata on backend links (default value: 15)
  uint8_t getLogicalSlot() const;

  //! Returns crate ID number for metadata on backend links (default value: 255)
  uint8_t getCrateCSPID() const;

protected:
  //! Set logical slot number (used for metadata on backend links). Only call if board is in ATCA crate.
  void setLogicalSlot(uint8_t);

  //! Set crate ID number for metadata on backend links
  void setCrateCSPID(uint8_t);

  void retrievePropertyValues() override;

private:
  static std::string convertBuildTimeToString(const BuildInfo::TimePoint_t&);

  static void setupLoggers();

  std::unique_ptr<emp::Controller> mController;

  uint8_t mLogicalSlot, mCrateCSPID;

  Property<std::string>& mFpgaType;
  Property<std::string>& mFpgaDNA;

  Property<float>& mPayloadClockFreq;

  Property<std::string>& mBuildTime;
  Property<std::string>& mBuildType;
  Property<::swatch::action::Table>& mBuildSourceAreas;

  SimpleMetric<float>& mTemperatureMetric;

  static std::once_flag sLogSetupOnceFlag;

  static const ConnectionCallback kDefaultConnectionCallback;
  static const InversionCallback kDefaultInversionCallback;
};

void populateTransitions(::swatch::phase2::PayloadTestFSM&);

void populateTransitions(::swatch::phase2::LinkTestFSM&);

void populateTransitions(::swatch::phase2::SliceTestFSM&);

} // namespace swatch
} // namespace emp

#endif

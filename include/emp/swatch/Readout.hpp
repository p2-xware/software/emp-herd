
#ifndef __EMP_SWATCH_READOUT_HPP__
#define __EMP_SWATCH_READOUT_HPP__


#include <memory>

#include "swatch/phase2/ReadoutInterface.hpp"


namespace emp {

class Controller;

namespace swatch {

class Readout : public ::swatch::phase2::ReadoutInterface {
public:
  Readout(const std::unique_ptr<emp::Controller>&);
  virtual ~Readout();

private:
  virtual void retrieveMetricValues();

  const std::unique_ptr<emp::Controller>& mController;
};

} // namespace swatch
} // namespace emp

#endif /* __EMP_SWATCH_READOUT_HPP__ */
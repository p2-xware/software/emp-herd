#ifndef __EMP_SWATCH_TXPORT_HPP__
#define __EMP_SWATCH_TXPORT_HPP__


#include <stddef.h>

#include "swatch/phase2/OutputPort.hpp"


namespace emp {

class Controller;

namespace swatch {

class TxPort : public ::swatch::phase2::OutputPort {
public:
  TxPort(const size_t, const std::unique_ptr<emp::Controller>&, const boost::optional<::swatch::phase2::ChannelID>&, bool aInvert);

  ~TxPort();

  bool invert() const;

private:
  static std::string createId(const size_t);

  bool checkPresence() const final;

  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  const std::unique_ptr<emp::Controller>& mController;
  const bool mInvertByDefault;

  Property<uint16_t>& mBufferDepth;
  Property<uint16_t>& mRefClockIndex;
  Property<std::string>& mMGTType;
  Property<std::string>& mLinkFirmwareType;
  Property<std::string>& mPolarity;
  Property<float>& mDriverDiffSwing;
  Property<float>& mDriverPreCursor;
  Property<float>& mDriverPostCursor;

  SimpleMetric<bool>& mMetricQpllLocked;
};

} // namespace swatch
} // namespace emp

#endif /* __EMP_SWATCH_TXPORT_HPP__ */
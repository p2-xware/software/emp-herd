
#ifndef __EMP_SWATCH_DUMMYSM_HPP__
#define __EMP_SWATCH_DUMMYSM_HPP__


#include <memory>

#include "swatch/phase2/ServiceModule.hpp"


namespace emp {
namespace swatch {
namespace test {

//! Dummy service module class, just created for CI tests
class DummySM : public ::swatch::phase2::ServiceModule {
public:
  DummySM(const ::swatch::core::AbstractStub& aStub);
  virtual ~DummySM();

private:
  void retrieveMetricValues() override;
};


} // namespace test
} // namespace swatch
} // namespace emp

#endif

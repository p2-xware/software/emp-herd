
#ifndef __EMP_SWATCH_TEST_DUMMYPROCESSOR_HPP__
#define __EMP_SWATCH_TEST_DUMMYPROCESSOR_HPP__


#include <memory>

#include "emp/swatch/Processor.hpp"


namespace emp {
namespace swatch {
namespace test {

//! Dummy processor class, just created for CI tests
class DummyProcessor : public Processor {
public:
  DummyProcessor(const ::swatch::core::AbstractStub& aStub);
  virtual ~DummyProcessor();
};


} // namespace test
} // namespace swatch
} // namespace emp

#endif

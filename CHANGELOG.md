# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.4.26] - 2024-10-13

## Changed

- Update to HERD version 0.4.25
- Update to EMP toolbox version 0.9.0


## [0.4.25] - 2024-09-25

### Fixed

- Update to HERD version 0.4.24 (fixes bugs in presence check of optical modules and missing tag in CI 'load plugin' test jobs)
- Update to EMP toolbox v0.8.4 (fixes issue with configuration of both PRBS rx & tx in the same channel)


## [0.4.24] - 2024-08-16

### Fixed

- Update to HERD version 0.4.22 (fixes property initialisation bug, adds uint16 & int16 parameter types)


## [0.4.23] - 2024-07-06

### Fixed

- Update to HERD version 0.4.21 (fixes CentOS7 YUM issues after EOL)


## [0.4.22] - 2024-04-21

### Fixed

- Tx ports: Fix bug in detection of CSP core


## [0.4.21] - 2024-04-07

### Changed

- Commands: Improve aliases and descriptions
- Update to HERD version 0.4.20 (adds smoke tests)
- CI pipeline: Add smoke tests


## [0.4.20] - 2024-03-14

### Changed

- Update to HERD version 0.4.18 (adds missing encoding/decoding functions for uint64_t, int64_t and double parameters)


## [0.4.19] - 2024-02-25

### Changed

- Update to HERD version 0.4.17 (updates almalinux base images)


## [0.4.18] - 2024-02-24

### Changed

- Update to HERD version 0.4.16 (empty algorithm interface now registered as fallback by SWATCH)


## [0.4.17] - 2024-01-15

### Changed

- Update to HERD version 0.4.15 (updates CMake version in CentOS7 image)


## [0.4.16] - 2024-01-12

### Changed

- Update to HERD version 0.4.14
  * Based off SWATCH v1.8.1, which fixes some uninitialised value errors, and updates CI templates
- Configure Rx MGTs command: Include connected optics channel in warning messages
- Program command base class: Add support for passing on path to additional config file.


## [0.4.15] - 2023-11-22

### Changed

- Update to HERD version 0.4.12
  * Based off SWATCH v1.8.0, which brings improvements relating to property updates


## [0.4.14] - 2023-11-20

### Changed

- Configure Tx MGTs command: Add 0.5 second sleep at the end to give time for optics to lock onto data after link firmware has been reset.


## [0.4.13] - 2023-11-03

### Fixed

- Update to HERD version 0.4.11
  * Fixes two bugs in post-action callbacks: failure of optics monitoring sync on boards with inter-FPGA links; deletion of local files


## [0.4.12] - 2023-10-29

### Changed

- Update to HERD version 0.4.10
  * Key changes: devtoolset-8 is now sourced outside the entrypoint of the CentOS7 amd64 and arm64 images; `lint:devcontainer` CI job.


## [0.4.11] - 2023-10-18

### Added

- Command that injects errors onto backend links
- Devcontainer config for developing in VSCode

### Changed

- Update to HERD version 0.4.9


## [0.4.10] - 2023-09-08

### Added

- Command that disables monitoring on FPGA I/O ports

### Fixed

- Slice test FSM: Disable FPGA I/O port monitoring at the end of the continue transition (input port errors are expected at that point since payload inputs are disabled, but ultimately the status of input ports is irrelevant at the end of that transition).


## [0.4.9] - 2023-08-07

### Changed

- Update to HERD version 0.4.8


## [0.4.8] - 2023-08-07

### Added

- Support for defining custom algo class in downstream repos

### Changed

- Configure RX MGTs command: Return error if idle method 2 requested

### Fixed

- Configure RX MGTs command: Fix logic error that prevents it from configuring multiple sets of channels


## [0.4.7] - 2023-07-31

### Fixed

- Update to HERD version 0.4.7, for connected port encoding bugfix


## [0.4.6] - 2023-07-30

### Changed

- Update to HERD version 0.4.6
- Update to EMP toolbox version 0.8.2


## [0.4.5] - 2023-07-21

### Changed

- Update to HERD version 0.4.4


## [0.4.4] - 2023-07-07

### Changed

- Update to EMP toolbox version 0.8.0

### Removed

- No longer support EMP firmware version 0.7.x


## [0.4.3] - 2023-06-08

### Fixed

- Update to HERD version 0.4.3


## [0.4.2] - 2023-05-25

### Fixed

- Update to HERD version 0.4.1


## [0.4.1] - 2023-05-22

### Changed

- Update to EMP toolbox version 0.7.9

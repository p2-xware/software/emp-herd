#!/bin/bash

set -e
set -x


UHAL_VERSION=2.8.16
EMP_VERSION=v0.9.4
#EMP_COMMIT=029ac724

DNF_CMD=dnf
REDHAT_RELEASE=$(cat /etc/redhat-release)
if [[ "${REDHAT_RELEASE}" == "CentOS Linux release 7."* ]]; then
  DNF_CMD=yum
  PYTHON_DEVEL_RPM=python36-devel
  IPBUS_REPO_FILE=ipbus-sw-centos7.repo
elif [[ "${REDHAT_RELEASE}" == "AlmaLinux release 8."* ]]; then
  PYTHON_DEVEL_RPM=python3.11-devel
  IPBUS_REPO_FILE=ipbus-sw.repo
elif [[ "${REDHAT_RELEASE}" == "AlmaLinux release 9."* ]]; then
  PYTHON_DEVEL_RPM=python3.11-devel
  IPBUS_REPO_FILE=ipbus-sw.repo
else
  echo "ERROR: OS not recognised. REDHAT_RELEASE=${REDHAT_RELEASE}"
  exit 1
fi


if [ "$1" == "app" ]
then
  ${DNF_CMD} -y install pugixml boost-date-time boost-chrono boost-regex boost-iostreams
  ${DNF_CMD} clean all

else

  # 1) Install dependencies of EMP toolbox
  cp ci/cactus-build-utils.repo /etc/yum.repos.d/
  cp ci/${IPBUS_REPO_FILE} /etc/yum.repos.d/ipbus-sw.repo
  ${DNF_CMD} clean all

  ${DNF_CMD} -y install --exclude *debuginfo \
    cactuscore-uhal-*-${UHAL_VERSION} \
    cactuscore-build-utils-0.3.5 \
    ${PYTHON_DEVEL_RPM} \
    git-core
#  cactusboards-emp-*-${EMP_VERSION}

  ${DNF_CMD} clean all

  # 2) Build EMP toolbox
  ORIGINAL_DIR=$(pwd)
  EMP_TOOLBOX_SOURCE_DIR=/tmp/_______________________________/emp-toolbox
  mkdir -p $(dirname ${EMP_TOOLBOX_SOURCE_DIR})
  git clone --recurse-submodules https://user:glpat-PNs2EZUoiwdzoAoHb6Qd@gitlab.cern.ch/p2-xware/software/emp-toolbox.git ${EMP_VERSION:+-b ${EMP_VERSION}} ${EMP_TOOLBOX_SOURCE_DIR}
  cd ${EMP_TOOLBOX_SOURCE_DIR}
  if [[ -n ${EMP_COMMIT} ]]; then
    git checkout ${EMP_COMMIT}
  fi

  make -j$(nproc)
  make rpm

  # 3) Install EMP toolbox
  #    (For arm/v7 image: Dependencies not installed in RPMs, so must use 'rpm' command with '--nodeps' option.)
  #    (Also add '--ignoresize' as size check erroneously fails on arm/v7.)
  rpm -iv --ignoresize --nodeps $(find $(pwd) -name "*.rpm" | grep -v debuginfo)

  # 4) Cleanup
  cd ${ORIGINAL_DIR}
  rm -rf ${EMP_TOOLBOX_SOURCE_DIR}
fi

# EMP SWATCH plugin

This repository contains a SWATCH plugin library for the EMP (Extensible, Modular, data Processor) firmware framework. Specifically, this library registers control/configuration procedures and monitoring data to the SWATCH framework, by wrapping short sections of code that invoke functions from the core EMP control/monitoring library (see below for links).

## Dependencies

The plugin library has two main dependencies:
 * [EMP software](http://serenity.web.cern.ch/serenity/emp-fwk/software/)
 * [SWATCH](https://gitlab.cern.ch/cms-cactus/core/swatch)


## Build instructions

To build the plugin, firstly clone this repository:
```sh
git clone ssh://git@gitlab.cern.ch:7999/p2-xware/software/emp-herd.git
```

The easiest way to build the plugin is inside a docker container, using the
[VSCode Dev Containers extension](https://code.visualstudio.com/docs/devcontainers/containers).
This repository includes a VSCode configuration file (`.devcontainer/devcontainer.json`) which
allows one to build in a development container that already contains all dependencies, along with
relevant tooling like git. To use this setup you just need to open the path for the cloned
repository in VSCode, click on the green bar in the bottom left of the window and then click
"Reopen in container". Instructions for other development environments can be found
[here](https://cms-l1t-phase2.docs.cern.ch/online-sw/plugin-development/development-environment.html).

After VSCode has downloaded & started the development container (or other development environment set
up), to build the plugin simply follow the standard CMake build workflow in the VScode terminal:
```sh
mkdir build
cd build
cmake3 ..
make -j$(nproc)
```


## Deployment

This plugin is designed as a base layer for the plugins of boards that use the EMP firmware
framework, and as such this plugin cannot be deployed on a board by itself. 

Instructions on how to deploy the corresponding off-board software (Shep) - and use it to control
and monitor boards - can be found in the [ShepHERD user guide](https://cms-l1t-phase2.docs.cern.ch/online-sw/user-guide/index.html)
